package com.calpoly.stylo.group;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import com.wacom.ink.willformat.aspects.ElementAspect;

import java.io.Serializable;
import java.lang.reflect.Member;
import java.util.Collection;
import java.util.List;

/**
 * Created by Jenna on 2/2/17.
 */

@Entity(tableName = "GroupTable")
public class Group implements Serializable {

    private String members;
    private String name;
    @Ignore
    private String adminId;
    @PrimaryKey
    private String id;


    @Ignore
    public Group(String id, String name, String members) {

        this.id = id;
        this.name = name;
        this.members = members;
    }

    public Group() { this.members = ""; }

    @Ignore
    public Group(String name) {
        this.name = name;
        this.members = "";
    }

    public String getId() { return id; }
    public String getName() { return name; }
    public String getMembers() { return members; }

    public boolean hasMembers() {
        return !members.equals("");
    }
    public void setName(String newName) {
        this.name = newName;
    }

    public void addMember(String newMember) {
        members = members + ", " + newMember;
    }

    public void setMembers(String members) {
        this.members = members;
    }

    public void setId(String newId) { this.id = newId;}

    public String toString() { return "GroupId: " + id + "\nGroupName: " + name + "\nAdminId: " + adminId + "\n";}

}
