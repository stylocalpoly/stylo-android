package com.calpoly.stylo.group;

import java.util.Collection;

/**
 * Created by Jenna on 2/2/17.
 */

public class User {
    private String firstName;
    private String lastName;
    private String id;
    private String email;
    private Collection<Group> groups;
    public static final String ID_ERROR = "UNKNOWN";

    public String getFirstName() { return firstName; }

    public String getLastName() { return lastName; }

    public String getId() { return id; }

    public String getEmail() { return email; }

    public Collection<Group> getGroups() { return groups; }

    // Constructors
    public User(){ }

    public User(String id, String firstName) {

        this.id = id;
        this.firstName = firstName;
    }

    public User(String id) {
        this.id = id;
    }

    public User(String id, String firstName, String lastName, String email) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }

    public String toString() {
        return firstName + " " + lastName;
    }
}
