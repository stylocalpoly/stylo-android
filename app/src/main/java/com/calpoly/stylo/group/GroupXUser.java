package com.calpoly.stylo.group;

import java.util.Collection;

/**
 * Created by Jenna on 2/2/17.
 */

public class GroupXUser {
  private String groupId;
  private String userId;
  private String id;
  public static final String ID_ERROR = "UNKNOWN";

  public String getUserId() { return userId; }

  public String getGroupId() { return groupId; }

  public String getId() { return id; }

  // Constructors
  public GroupXUser(){ }

  public GroupXUser(String userId, String groupId){
    this.groupId = groupId;
    this.userId = userId;
  }

  public GroupXUser(String id, String userId, String groupId) {

    this.id = id;
    this.groupId = groupId;
    this.userId = userId;
  }

  public GroupXUser(String id) {
    this.id = id;
  }

  public String toString() {
    return userId + " -> " + groupId;
  }
}
