package com.calpoly.stylo.group;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by cube on 5/26/2017.
 */

@Entity
public class CurrentUser {
    private String firstName;
    private String lastName;
    @PrimaryKey
    private String id;
    private String email;

    public CurrentUser(){}

    @Ignore
    public CurrentUser(String fName, String lName, String myID, String emailAddress)
    {
        firstName = fName;
        lastName = lName;
        id = myID;
        email = emailAddress;
    }

    public CurrentUser(User usr) {
        firstName = usr.getFirstName();
        lastName = usr.getLastName();
        id = usr.getId();
        email = usr.getEmail();
    }

    public String getFirstName() {
        return firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public String getId() {
        return id;
    }
    public String getEmail() {
        return email;
    }
    public void setFirstName(String fName) {
        firstName = fName;
    }
    public void setLastName(String lName) {
        lastName = lName;
    }
    public void setId(String myID) {
        id = myID;
    }
    public void setEmail(String emailAddress) {
        email = emailAddress;
    }
}