package com.calpoly.stylo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;

import android.content.Context;
import android.content.SharedPreferences;

import com.calpoly.stylo.azure.AppDAO;
import com.calpoly.stylo.azure.OAuthService;
import com.calpoly.stylo.azure.Session;
import com.calpoly.stylo.group.CurrentUser;
import com.calpoly.stylo.group.User;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.microsoft.windowsazure.mobileservices.MobileServiceActivityResult;
import com.microsoft.windowsazure.mobileservices.authentication.MobileServiceAuthenticationProvider;


public class LoginActivity extends AppCompatActivity {
    public static final String SHAREDPREFFILE = "temp";
    public static final String GET_USER_FAIL = "Couldn't get user.";
    protected boolean shouldDelete = true;
    public static final int GOOGLE_LOGIN_REQUEST_CODE = 11;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ImageButton facebookButton = (ImageButton) findViewById(R.id.facebookbutton);
        ImageButton googleButton = (ImageButton) findViewById(R.id.googlebutton);
        ImageButton microsoftButton = (ImageButton) findViewById(R.id.microsoftbutton);

        SharedPreferences prefs = getSharedPreferences(SHAREDPREFFILE, Context.MODE_PRIVATE);

        final OAuthService OAuth = new OAuthService(prefs, this);

        Session.initialize(OAuth);

         // Check if they have already authenticated.
         if(OAuth.hasToken()) {
              OAuth.loadToken();
              AppDAO.initialize(this);
              shouldDelete = false;
              AppDAO.getCurrentUser(loginCallback);
         }

        // We do not care when the future finishes, since there are a lot of async things
        // that happen. The LoginActivity is just listening for when the Session is "ready."
        facebookButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login(MobileServiceAuthenticationProvider.Facebook);
            }
        });

        googleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Session.client.login("Google", "stylo", GOOGLE_LOGIN_REQUEST_CODE);
            }
        });

        microsoftButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login(MobileServiceAuthenticationProvider.MicrosoftAccount);
            }
        });
    }

    public void openGroupListActivity(){
        startActivity(new Intent(this, GroupListActivity.class));
    }

    /**
     * Login the user and add the login callback
     *
     * @param provider the login provider for OAuth.
     */
    private void login(MobileServiceAuthenticationProvider provider) {
        Futures.addCallback(Session.login(provider), loginCallback);
    }

    /**
     * The callback to use for getting the user.
     */
    private FutureCallback<User> loginCallback = new FutureCallback<User>() {

        @Override
        public void onSuccess(User user) {
            // Set the user object and open the group list
            Session.user = user;
            AppDAO.initialize(getApplicationContext());
            if (shouldDelete) {
                AppDAO.clearTables();
            }
            AppDAO.setCurrentUser(new CurrentUser(Session.user), setCallback);
        }

        @Override
        public void onFailure(Throwable throwable) {
            if (throwable.getMessage().equals(GET_USER_FAIL)){
                Futures.addCallback(Session.getUser(), loginCallback);
            }
            else {
                throwable.printStackTrace();
            }
        }
    };

    protected FutureCallback setCallback = new FutureCallback() {
        @Override
        public void onSuccess(Object o) {
            openGroupListActivity();
        }

        @Override
        public void onFailure(Throwable throwable) {
            throwable.printStackTrace();
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // When request completes
        if (resultCode == RESULT_OK) {
            // Check the request code matches the one we send in the login request
            if (requestCode == GOOGLE_LOGIN_REQUEST_CODE) {
                MobileServiceActivityResult result = Session.client.onActivityResult(data);
                if (result.isLoggedIn()) {
                    // login succeeded
                    Log.d("LOGIN", "Successful");
                    Futures.addCallback(Session.googleLogin(), loginCallback);
                } else {
                    // login failed, check the error message
                    Log.d("LOGIN", "Unsuccessful");
                }
            }
        }
    }
}
