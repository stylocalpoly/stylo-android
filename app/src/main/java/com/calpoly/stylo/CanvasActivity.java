package com.calpoly.stylo;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import com.calpoly.stylo.azure.AppDAO;
import com.calpoly.stylo.azure.Blob;
import com.calpoly.stylo.azure.Message;
import com.calpoly.stylo.azure.OAuthService;
import com.calpoly.stylo.azure.Session;
import com.calpoly.stylo.canvas.CanvasBitmap;
import com.calpoly.stylo.document.DocumentManager;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.SurfaceTexture;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Size;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.TextureView;
import android.view.View.OnTouchListener;
import android.view.View;
import android.view.MotionEvent;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.calpoly.stylo.canvas.CanvasElement;
import com.calpoly.stylo.canvas.CanvasImage;
import com.calpoly.stylo.canvas.CanvasRenderer;
import com.calpoly.stylo.canvas.CanvasStroke;
import com.calpoly.stylo.canvas.CanvasTool;
import com.calpoly.stylo.canvas.ElementFactory;
import com.calpoly.stylo.canvas.Selection;
import com.calpoly.stylo.utils.AppUtils;
import com.google.common.collect.ImmutableList;
import com.wacom.ink.manipulation.Intersector;
import com.wacom.ink.path.SpeedPathBuilder;
import com.wacom.ink.path.PathUtils.Phase;
import com.wacom.ink.path.PathUtils;
import com.wacom.ink.path.PathBuilder.PropertyFunction;
import com.wacom.ink.path.PathBuilder.PropertyName;
import com.wacom.ink.rasterization.Layer;
import com.wacom.ink.smooth.MultiChannelSmoothener;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.nio.FloatBuffer;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Stack;

public class CanvasActivity extends AppCompatActivity {

    private TextureView textureView;

    private CanvasRenderer canvasRenderer;
    private DocumentManager manager;

    private Stack<ImmutableList<CanvasElement>> undoHistory = new Stack<ImmutableList<CanvasElement>>();

    private Layer currentLayer;

    private SpeedPathBuilder pathBuilder;

    private Intersector<CanvasElement> intersector;
    private MultiChannelSmoothener smoothener;
    private CanvasTool currentTool;

    private Stack<CanvasElement> renderStack = new Stack<CanvasElement>();

    private int currentColor;

    private boolean moveElement;

    private int selectButtonImage;

    private static final String storageConnectionString =
            "DefaultEndpointsProtocol=http;" +
                    "AccountName=penmessagestorage;" +
                    "AccountKey=BV5WR1Km404XR6K8F/KxOKuAyTw0utckHVZvOqW/LO5+cUTNVdZ9hShhBS/oOR7VAjKaSlt9+nBVVLXdvRpCgQ==";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_canvas);

        manager = new DocumentManager(getApplicationContext());


        selectButtonImage = R.drawable.ic_crop_free_black_24dp;

        Selection.initialize();
        moveElement = false;

        //when selecting and item, hitting back and opening canvas again selection box still shows
        //therefore must refresh selection box
        if (Selection.isSelected()) {
            Selection.deselect();
        }

        currentTool = CanvasTool.DRAW_TOOL;
        currentColor = Color.BLUE;

        // Create the path builder
        pathBuilder = new SpeedPathBuilder();
        pathBuilder.setNormalizationConfig(100.0f, 4000.0f);
        pathBuilder.setMovementThreshold(2.0f);
        pathBuilder.setPropertyConfig(PropertyName.Width, 5f, 10f, 5f, 10f, PropertyFunction.Power, 1.0f, false);
        intersector = new Intersector<CanvasElement>();

        textureView = (TextureView) findViewById(R.id.textureView);


        textureView.setSurfaceTextureListener(new TextureView.SurfaceTextureListener() {
            @Override
            public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {

                assert width == height;
                Log.d("CanvasActivity", "intialized.");

                Size dimens = new Size(width, height);

                if(getIntent().hasExtra("source")) {
                    Log.d("onCreate", "cloning the canvas to source.");
                    File source = (File) getIntent().getSerializableExtra("source");
                    manager.loadWillFile(renderStack, source, dimens, getResources().getDisplayMetrics().density);
                }


                canvasRenderer = new CanvasRenderer(surface, dimens);
                currentLayer = canvasRenderer.createLayer();
                canvasRenderer.render(currentLayer, renderStack);
                smoothener = new MultiChannelSmoothener(pathBuilder.getStride());
                smoothener.enableChannel(2);
            }

            @Override
            public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {

            }

            @Override
            public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
                // The Surface is being destroyed so release the resources within
                canvasRenderer.releaseResources();
                return false;
            }

            @Override
            public void onSurfaceTextureUpdated(SurfaceTexture surface) {

            }
        });


        textureView.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                boolean doneBuilding = event.getAction() == MotionEvent.ACTION_UP;

                if (event.getAction() != MotionEvent.ACTION_DOWN
                        && event.getAction() != MotionEvent.ACTION_MOVE
                        && !doneBuilding) {
                    return false;
                }
                // Continue building the path until it is done.
                buildPath(event);

                switch(currentTool) {
                    case DRAW_TOOL:
                        canvasRenderer.renderLive(currentLayer, event, pathBuilder);
                        // If we are done with the stroke, save the render stack then add the stroke and render.
                        if (doneBuilding) {
                            addToHistory(renderStack);
                            renderStack.add(ElementFactory.createStroke(pathBuilder, canvasRenderer));
                        }
                        break;
                    case TEXT_TOOL:
                        break;
                    case ERASE_TOOL:
                        erase(doneBuilding);
                        break;
                    case SELECT_TOOL:
                        //nothing selected yet so not moving
                        if (!moveElement && !doneBuilding) {
                            select();
                        } else if (doneBuilding) {
                            if (moveElement) {
                                moveElement = false;
                                moveElements(event.getX(), event.getY());
                            } else if (Selection.isSelected()) {
                                moveElement = true;
                            }
                        } else if (moveElement && !doneBuilding) {
                            if (Selection.isSelected()) {
                                canvasRenderer.renderMoving(event, Selection.getSelected(), renderStack, findViewById(R.id.textureView).getHeight());
                            }
                        }
                        break;
                    case DELETE_TOOL:
                        break;
                }
                return true;
            }
        });

        final ImageButton selectionButton = (ImageButton) findViewById(R.id.selectionbutton);
        selectionButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                //change image of button from select to trashcan
                selectButtonImage = R.drawable.ic_delete_black_24dp;
                selectionButton.setColorFilter(Color.RED);
                selectionButton.setImageResource(selectButtonImage);
                currentTool = CanvasTool.SELECT_TOOL;
                //if an item is selected and button is pushed, delete selected item
                if (Selection.isSelected()) {
                    //save the render stack
                    addToHistory(renderStack);
                    for (CanvasElement element : Selection.getSelected()) {
                        if (renderStack.contains(element)) {
                            renderStack.remove(element);
                        }
                    }
                    //refresh canvas view
                    Selection.deselect();
                    canvasRenderer.clear(currentLayer);
                    canvasRenderer.render(currentLayer, renderStack);
                }
                else {
                    clearSelectedButton();
                    hideEditTextButton();
                    hideColorPalette();
                }
            }
        });

        final ImageButton addStrokeButton = (ImageButton) findViewById(R.id.drawbutton);
        addStrokeButton.setColorFilter(sanitize(R.color.stylo_green));
        addStrokeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearSelectedButton();
                currentTool = CanvasTool.DRAW_TOOL;
                addStrokeButton.setColorFilter(sanitize(R.color.stylo_green));
                canvasRenderer.setColor(currentColor);
                hideEditTextButton();
                hideColorPalette();
                selectionButton.setImageResource(R.drawable.ic_crop_free_black_24dp);
            }
        });

        // Find the import button and assign it an action
        ImageButton importImageButton = (ImageButton) findViewById(R.id.importimagebutton);
        importImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearSelectedButton();
                // Start a new intent to select an image
                Intent photoPicker = new Intent(Intent.ACTION_GET_CONTENT);
                photoPicker.setType("image/*");
                startActivityForResult(photoPicker, AppUtils.REQUEST_CODE_IMAGE);
                hideEditTextButton();
                hideColorPalette();
                selectionButton.setImageResource(R.drawable.ic_crop_free_black_24dp);
            }
        });

        //resize selected items up
        ImageButton resizeUp = (ImageButton) findViewById(R.id.resizeUp);
        resizeUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resizeElements(true);
                hideEditTextButton();
                hideColorPalette();
            }
        });

        //resize selected items down
        ImageButton resizeDown = (ImageButton) findViewById(R.id.resizeDown);
        resizeDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resizeElements(false);
                hideEditTextButton();
                hideColorPalette();
            }
        });

        final ImageButton addTextButton = (ImageButton) findViewById(R.id.addtextbutton);
        addTextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearSelectedButton();
                currentTool = CanvasTool.TEXT_TOOL;
                addTextButton.setColorFilter(sanitize(R.color.stylo_green));
                final EditText editText = (EditText) findViewById(R.id.editText);
                editText.setVisibility(View.VISIBLE);

                final Button setTextButton = (Button) findViewById(R.id.setTextButton);
                setTextButton.setVisibility(View.VISIBLE);
                setTextButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        addToHistory(renderStack);

                        String text = editText.getText().toString();
                        if (text.trim().length() > 0) {
                            renderStack.add(ElementFactory.createTextFromString(text));

                            canvasRenderer.render(currentLayer, renderStack);
                        }
                        setTextButton.setVisibility(View.INVISIBLE);
                        editText.setVisibility(View.INVISIBLE);
                        editText.setText("");

                        // Check if no view has focus:
                        editText.clearFocus();
                    }
                });
                hideColorPalette();
                selectionButton.setImageResource(R.drawable.ic_crop_free_black_24dp);
            }
        });

        ImageButton undoButton = (ImageButton) findViewById(R.id.undobutton);
        undoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearSelectedButton();
                //remove selction box if an item is undone
                if (Selection.isSelected()) {
                    Selection.deselect();
                    canvasRenderer.clear(currentLayer);
                    canvasRenderer.render(currentLayer, renderStack);
                }
                canvasRenderer.clear(currentLayer);
                renderStack = new Stack<CanvasElement>();

                if (!undoHistory.isEmpty()) {
                    renderStack.addAll(undoHistory.pop());
                }

                canvasRenderer.render(currentLayer, renderStack);
                hideEditTextButton();
                hideColorPalette();
                selectionButton.setImageResource(R.drawable.ic_crop_free_black_24dp);
            }
        });

        final ImageButton eraseButton = (ImageButton) findViewById(R.id.erasebutton);
        eraseButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                clearSelectedButton();
                currentTool = CanvasTool.ERASE_TOOL;
                eraseButton.setColorFilter(sanitize(R.color.stylo_green));
                Log.d("IMAGE", "image button pressed, index = " + renderStack.size());
                hideEditTextButton();
                hideColorPalette();
                selectionButton.setImageResource(R.drawable.ic_crop_free_black_24dp);
            }
        });

        final ImageButton paletteButton = (ImageButton) findViewById(R.id.palettebutton);
        paletteButton.setColorFilter(currentColor);
        paletteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearSelectedButton();
                currentTool = CanvasTool.DRAW_TOOL;

                //make all buttons for palette visible and change stroke colors
                final Button brownButton = (Button) findViewById(R.id.brownbutton);
                brownButton.setVisibility(View.VISIBLE);
                brownButton.setBackgroundColor(Color.rgb(102, 51, 0));
                brownButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        setColor(sanitize(R.color.brown));
                        paletteButton.setColorFilter(currentColor);
                    }
                });

                final Button redButton = (Button) findViewById(R.id.redbutton);
                redButton.setVisibility(View.VISIBLE);
                redButton.setBackgroundColor(Color.RED);
                redButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        setColor(Color.RED);
                        paletteButton.setColorFilter(currentColor);
                    }
                });

                final Button orangeButton = (Button) findViewById(R.id.orangebutton);
                orangeButton.setVisibility(View.VISIBLE);
                orangeButton.setBackgroundColor(Color.rgb(255, 128, 0));
                orangeButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        setColor(sanitize(R.color.orange));
                        paletteButton.setColorFilter(currentColor);
                    }
                });

                final Button yellowButton = (Button) findViewById(R.id.yellowbutton);
                yellowButton.setVisibility(View.VISIBLE);
                yellowButton.setBackgroundColor(Color.YELLOW);
                yellowButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        setColor(Color.YELLOW);
                        paletteButton.setColorFilter(currentColor);
                    }
                });

                final Button greenButton = (Button) findViewById(R.id.greenbutton);
                greenButton.setVisibility(View.VISIBLE);
                greenButton.setBackgroundColor(Color.GREEN);
                greenButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        setColor(Color.GREEN);
                        paletteButton.setColorFilter(currentColor);
                    }
                });

                final Button blueButton = (Button) findViewById(R.id.bluebutton);
                blueButton.setVisibility(View.VISIBLE);
                blueButton.setBackgroundColor(Color.BLUE);
                blueButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        setColor(Color.BLUE);
                        paletteButton.setColorFilter(currentColor);
                    }
                });

                final Button purpleButton = (Button) findViewById(R.id.purplebutton);
                purpleButton.setVisibility(View.VISIBLE);
                purpleButton.setBackgroundColor(Color.rgb(102, 0, 204));
                purpleButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        setColor(sanitize(R.color.purple));
                        paletteButton.setColorFilter(currentColor);
                    }
                });

                final Button blackButton = (Button) findViewById(R.id.blackbutton);
                blackButton.setVisibility(View.VISIBLE);
                blackButton.setBackgroundColor(Color.BLACK);
                blackButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        setColor(Color.BLACK);
                        paletteButton.setColorFilter(currentColor);
                    }
                });

                final Button grayButton = (Button) findViewById(R.id.graybutton);
                grayButton.setVisibility(View.VISIBLE);
                grayButton.setBackgroundColor(Color.GRAY);
                grayButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        setColor(Color.GRAY);
                        paletteButton.setColorFilter(currentColor);
                    }
                });

                final Button whiteButton = (Button) findViewById(R.id.whitebutton);
                whiteButton.setVisibility(View.VISIBLE);
                whiteButton.setBackgroundColor(Color.WHITE);
                whiteButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        setColor(Color.WHITE);
                        paletteButton.setColorFilter(currentColor);
                    }
                });

                hideEditTextButton();
                selectionButton.setImageResource(R.drawable.ic_crop_free_black_24dp);
            }
        });

        //Added for save
        final FloatingActionButton sendButton = (FloatingActionButton) findViewById(R.id.sendButton);
        sendButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                final HashMap<String, String> metadata = new HashMap();
                metadata.put("timestamp", String.valueOf(Calendar.getInstance().getTimeInMillis()));
                metadata.put("sender", Session.msUser.getUserId());
                metadata.put("username", Session.user.getFirstName());

                File location = getDir(Session.currentGroup, MODE_PRIVATE);
                String fileName = Blob.makeName(metadata);
                save(Session.currentGroup, fileName);
                Log.d("CANVASSAVE", location + " / " + fileName);
                Message newMessage = new Message(new File(location, fileName), metadata, Session.currentGroup);

                renderStack.clear();
                canvasRenderer.clear(currentLayer);
                canvasRenderer.render(currentLayer, renderStack);

                AppDAO.sendMessage(newMessage);
                finish();
            }
        });
    }

    //moves the selected elements
    private void moveElements(float x, float y) {
        if (Selection.isSelected()) {
            //save the render stack
            addToHistory(renderStack);
            for (CanvasElement element : Selection.getSelected()) {
                CanvasElement newElement = element;
                //for images and text
                if (element instanceof CanvasBitmap) {
                    newElement = new CanvasBitmap((CanvasBitmap) element);
                    //moves elements a set amount
                    ((CanvasBitmap) newElement).moveElement(x, y);
                    //calculate bounds occurs in cavnasRenderer for bitmaps
                }
                else { //for strokes
                    newElement = new CanvasStroke((CanvasStroke) element);
                    //move the element a set amount
                    ((CanvasStroke) newElement).moveElement(x, y, findViewById(R.id.textureView).getHeight());
                    ((CanvasStroke) newElement).calculateBounds();
                }
                renderStack.remove(element);
                renderStack.add(newElement);
            }
            Selection.deselect();
            canvasRenderer.clear(currentLayer);
            canvasRenderer.render(currentLayer, renderStack);
        }
    }

    //resize the selected elements
    private void resizeElements(boolean makeBigger) {
        if (Selection.isSelected()) {
            //save the render stack
            addToHistory(renderStack);
            Stack<CanvasElement> newElementsToSelect = new Stack<CanvasElement>();
            for (CanvasElement element : Selection.getSelected()) {
                CanvasElement newElement = element;
                //for images and text
                if (element instanceof CanvasBitmap) {
                    newElement = new CanvasBitmap((CanvasBitmap) element);
                    ((CanvasBitmap) newElement).resizeElement(makeBigger);
                    //calculate bounds occurs in cavnasRenderer for bitmaps
                }
                else { //for strokes
                    newElement = new CanvasStroke((CanvasStroke) element);
                    ((CanvasStroke) newElement).resizeElement(makeBigger, findViewById(R.id.textureView).getHeight());
                    ((CanvasStroke) newElement).calculateBounds();
                }
                renderStack.remove(element);
                renderStack.add(newElement);
                newElementsToSelect.add(newElement);
            }
            Selection.deselect();
            for (CanvasElement element : newElementsToSelect) {
                Selection.add(element);
            }
            canvasRenderer.clear(currentLayer);
            canvasRenderer.render(currentLayer, renderStack);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater mInflater = getMenuInflater();
        mInflater.inflate(R.menu.canvas_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save_draft:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);

                final View fView = getLayoutInflater().inflate(R.layout.field_layout, null);
                builder.setView(fView);

                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String fname = ((EditText)fView.findViewById(R.id.dialog_field)).getText().toString().trim();
                        if (fname.length() > 0) {
                            save("drafts", fname);
                        }
                    }
                });
                builder.setNegativeButton("Close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                builder.show();
                return true;
            case R.id.load_draft:
                AlertDialog.Builder loadBuilder = new AlertDialog.Builder(this);
                loadBuilder.setTitle("Load Draft");
                final View loadView = getLayoutInflater().inflate(R.layout.field_layout, null);
                loadBuilder.setView(loadView);

                loadBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String fname = ((EditText)loadView.findViewById(R.id.dialog_field)).getText().toString().trim();
                        if (fname.length() > 0) {
                            load("drafts", fname);
                        }
                    }
                });
                loadBuilder.setNegativeButton("Close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                loadBuilder.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void save(String folder, String path) {
        final File dir = getDir(folder, MODE_PRIVATE);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        final File source = new File(dir, path);

        float density = getResources().getDisplayMetrics().density;

        manager.saveWillFile(
                renderStack,
                source,
                canvasRenderer.size,
                textureView.getBitmap(), density
        );
        Log.d("SAVINGCACHEOFFLINE", source.toString());

        if (folder.equals("drafts")) {
            Toast.makeText(this, "Draft '" + path + "' saved.", Toast.LENGTH_SHORT).show();
        }
        else {
            Selection.deselect();
            renderStack.clear();
            canvasRenderer.clear(currentLayer);
            canvasRenderer.render(currentLayer, renderStack);
        }
    }

    public void load(String folder, String path) {
        final File dir = getDir(folder, MODE_PRIVATE);
        final File source = new File(dir, path);
        renderStack.clear();
        canvasRenderer.clear();
        manager.loadWillFile(renderStack, source, canvasRenderer.size, getResources().getDisplayMetrics().density);
        canvasRenderer.render(currentLayer, renderStack);
    }

    /*
     * Set the canvasRenderer color to the color selected and save the color in currentColor.
     */
    private void setColor(int color) {
        canvasRenderer.setColor(color);
        currentColor = color;
    }

    private void erase(boolean doneBuilding) {
        // If we are done drawing and there are elements to erase.
        if (doneBuilding && !renderStack.isEmpty()) {
            intersector.setTargetAsClosedPath(pathBuilder.getPathBuffer(), 0, pathBuilder.getPathSize(), pathBuilder.getStride());
            //strokes that will be removed
            LinkedList<CanvasElement> removedStrokes = new LinkedList<>();
            //strokes that will be added due to strokes being split in half from erasing
            LinkedList<CanvasElement> newStrokes = new LinkedList<>();
            //indexes of the erased strokes so they can be put back in proper order to render
            LinkedList<Integer> indexes = new LinkedList<Integer>();

            for (CanvasElement element : renderStack) {
                //Erasing only works on strokes.
                if (intersector.isIntersectingTarget(element) && element instanceof CanvasStroke) {
                    CanvasStroke stroke = (CanvasStroke) element;
                    Intersector.IntersectionResult intersection = intersector.intersectWithTarget(stroke);
                    removedStrokes.add(stroke);
                    Intersector.IntervalIterator iterator = intersection.getIterator();

                    //get all new strokes, since partially erased strokes get broken into more
                    while (iterator.hasNext()) {
                        Intersector.Interval interval = iterator.next();
                        if (!interval.inside) {
                            //create new stroke to render
                            int size = interval.toIndex - interval.fromIndex + stroke.getStride();
                            CanvasStroke newStroke = new CanvasStroke(size);
                            newStroke.copyPoints(stroke.getPoints(), interval.fromIndex, size);
                            newStroke.setStride(stroke.getStride());
                            newStroke.setColor(stroke.getColor());
                            newStroke.setBlendMode(stroke.getBlendMode());
                            newStroke.setWidth(stroke.getWidth());
                            newStroke.setInterval(interval.fromValue, interval.toValue);
                            newStroke.calculateBounds();
                            newStrokes.add(newStroke);
                            //store index of orignal stroke so we know where to insert new stroke
                            indexes.add(renderStack.indexOf(stroke));
                        }
                    }
                }
            }

            // Save the render stack right before erasing.
            addToHistory(renderStack);
            //go backwards through list of new strokes to be inserted, this ensures they
            // are placed at proper index since the stack is growing with each insertion
            for (int i = newStrokes.size() - 1; i >= 0; i--) {
                renderStack.insertElementAt(newStrokes.get(i), indexes.get(i));
            }
            renderStack.removeAll(removedStrokes);
            canvasRenderer.clear(currentLayer);
            canvasRenderer.render(currentLayer, renderStack);
        }
    }

    private void select() {
        intersector.setTargetAsClosedPath(
                pathBuilder.getPathBuffer(),
                0,
                pathBuilder.getPathSize(),
                pathBuilder.getStride());

        for (CanvasElement element : renderStack) {
            if (intersector.isIntersectingTarget(element) && !Selection.getSelected().contains(element)) {
                Selection.add(element);
            }
        }

        //render the selection box
        if (Selection.isSelected()) {
            canvasRenderer.clear(currentLayer);
            canvasRenderer.render(currentLayer, renderStack);
        }
    }

    private void clearSelectedButton() {
        ((ImageButton) findViewById(currentTool.getId()))
                .setColorFilter(Color.TRANSPARENT);
        selectButtonImage = R.drawable.ic_crop_free_black_24dp;
        //if switching tools, remove selection box because we are no longer in select mode
        if (Selection.isSelected()) {
            Selection.deselect();
            canvasRenderer.clear(currentLayer);
            canvasRenderer.render(currentLayer, renderStack);
        }
    }


    private void addToHistory(Stack<CanvasElement> elements) {
        undoHistory.push(ImmutableList.copyOf(elements));
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    /**
     * Called upon returning to this activity after starting a different activity for a result.
     *
     * @param requestCode         The unique request code that was sent for a specific action
     * @param resultCode          The result code, either RESULT_OK or RESULT_CANCELLED
     * @param imageReturnedIntent The intent containing the results of the activity
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        switch (requestCode) {
            case AppUtils.REQUEST_CODE_IMAGE:
                if (resultCode == RESULT_OK) {
                    try {
                        addToHistory(renderStack);
                        final Uri imageUri = imageReturnedIntent.getData();
                        final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                        CanvasImage canvasImg = ElementFactory.createImageFromStream(imageStream);
                        canvasImg.x = (canvasRenderer.size.getWidth() - canvasImg.width) / 2;
                        canvasImg.y = (canvasRenderer.size.getHeight() - canvasImg.height) / 2;
                        renderStack.add(canvasImg);

                        currentTool = CanvasTool.DRAW_TOOL;

                        canvasRenderer.clear(currentLayer);
                        canvasRenderer.render(currentLayer, renderStack);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }
        }
    }

    /**
     * Builds the path the user is tracking into the pathbuilder.
     *
     * @param event
     */
    private void buildPath(MotionEvent event) {
        if (event.getAction()==MotionEvent.ACTION_DOWN){
            // Reset the smoothener instance when starting to generate a new path.
            smoothener.reset();
        }

        Phase phase = PathUtils.getPhaseFromMotionEvent(event);
        // Add the current input point to the path builder
        FloatBuffer part = pathBuilder.addPoint(phase, event.getX(), event.getY(), event.getEventTime());
        MultiChannelSmoothener.SmoothingResult smoothingResult;
        int partSize = pathBuilder.getPathPartSize();

        if (AppUtils.hasPartSize(pathBuilder)) {
            // Smooth the returned control points (aka partial path).
            smoothingResult = smoothener.smooth(part, partSize, (phase==Phase.END));
            // Add the smoothed control points to the path builder.
            pathBuilder.addPathPart(smoothingResult.getSmoothedPoints(), smoothingResult.getSize());
        }
    }

    /**
     * Does a compatibility check on a color we have in our theme.
     *
     * @param color
     * @return the resource color value.
     */
    private int sanitize(int color) {
        return ContextCompat.getColor(getApplicationContext(), color);
    }

    /*
     * This method sets the edit text button to invisible.
     * That way they are only displayed when using the text tool.
     */
    private void hideEditTextButton() {
        final EditText editText = (EditText) findViewById(R.id.editText);
        editText.setVisibility(View.INVISIBLE);
        final Button setTextButton = (Button) findViewById(R.id.setTextButton);
        setTextButton.setVisibility(View.INVISIBLE);
    }

    /*
     * This method sets all of the color palette buttons to invisible.
     * That way they are only displayed when using the palette tool.
     */
    private void hideColorPalette() {
        final Button brown = (Button) findViewById(R.id.brownbutton);
        brown.setVisibility(View.INVISIBLE);
        final Button red = (Button) findViewById(R.id.redbutton);
        red.setVisibility(View.INVISIBLE);
        final Button orange = (Button) findViewById(R.id.orangebutton);
        orange.setVisibility(View.INVISIBLE);
        final Button yellow = (Button) findViewById(R.id.yellowbutton);
        yellow.setVisibility(View.INVISIBLE);
        final Button green = (Button) findViewById(R.id.greenbutton);
        green.setVisibility(View.INVISIBLE);
        final Button blue = (Button) findViewById(R.id.bluebutton);
        blue.setVisibility(View.INVISIBLE);
        final Button purple = (Button) findViewById(R.id.purplebutton);
        purple.setVisibility(View.INVISIBLE);
        final Button black = (Button) findViewById(R.id.blackbutton);
        black.setVisibility(View.INVISIBLE);
        final Button gray = (Button) findViewById(R.id.graybutton);
        gray.setVisibility(View.INVISIBLE);
        final Button white = (Button) findViewById(R.id.whitebutton);
        white.setVisibility(View.INVISIBLE);
    }

    /**
     * A helper method for tests. Do not use in production code.
     * @return
     */
    Stack<ImmutableList<CanvasElement>> getUndoHistory() {
        return undoHistory;
    }

    Stack<CanvasElement> getRenderStack() {
        return renderStack;
    }

    CanvasTool getCanvasTool() {
        return currentTool;
    }

}
