package com.calpoly.stylo;

import android.app.Dialog;
import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleActivity;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.LifecycleRegistryOwner;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.OnLifecycleEvent;
import android.arch.lifecycle.ViewModel;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.calpoly.stylo.azure.AppDAO;
import com.calpoly.stylo.azure.MyNotificationHandler;
import com.calpoly.stylo.azure.NotificationSettings;
import com.calpoly.stylo.azure.OAuthService;
import com.calpoly.stylo.azure.RegistrationIntentService;
import com.calpoly.stylo.azure.Session;
import com.calpoly.stylo.group.Group;
import com.calpoly.stylo.group.User;
import com.microsoft.windowsazure.notifications.NotificationsManager;

//notification imports
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Exchanger;

/**
 * An activity representing a list of Groups. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link GroupDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
public class GroupListActivity extends AppCompatActivity implements LifecycleRegistryOwner, Observer<User[]> {
    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    public static boolean mTwoPane;
    public static final int GROUP_VIEW = 1;
    private GroupAdapter adapter;
    private GroupAdapter.GroupViewHolder previousSelectedGroup = null;

    private RecyclerView recyclerView;

    //notification objects
    //public static MainActivity mainActivity;
    public static Boolean isVisible = false;
    private static final String TAG = "MainActivity";
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    private final LifecycleRegistry mRegistry = new LifecycleRegistry(this);


    @Override
    public void onResume() {
        super.onResume();
        isVisible = true;
        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // if this view has state saved after being killed
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_list);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getTitle());

        // TODO: Add the toolbar back somehow
        setSupportActionBar(toolbar);
        ImageButton createGroupButton = (ImageButton) findViewById(R.id.create_group_button);
        createGroupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(GroupListActivity.this);
                dialog.setContentView(R.layout.create_group_dialog);

                final EditText groupNameInput = (EditText) dialog.findViewById(R.id.edit_text);

                final Button ok = (Button) dialog.findViewById(R.id.finish_group_creation_button);
                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // if there isn't a group name entered
                        if (groupNameInput.getText().length() > 0) {
                            Session.createGroup(new Group(groupNameInput.getText().toString()), view.getContext());
                            dialog.dismiss();
                        }
                        else if(groupNameInput.getText().length() == 0) {
                            Toast.makeText(getApplicationContext(),"Groups must have a name.",Toast.LENGTH_SHORT).show();
                        }
                        else {
                            dialog.dismiss();
                        }
                    }
                });

                final Button cancel = (Button) dialog.findViewById(R.id.cancel_group_creation_button);
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.group_list);
        assert recyclerView != null;

        // When this finishes, it will trigger onGroups() and the recycler view will be created.
        //Session.getGroups();
        LiveData<Group[]> groupData =  AppDAO.getGroups();
        Log.d("GetGroup Call", groupData.toString());


        if (findViewById(R.id.group_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            mTwoPane = true;
        }

        //send notifications
        NotificationsManager.handleNotifications(this, NotificationSettings.SenderId, MyNotificationHandler.class);
        registerWithNotificationHubs();

        if (ContextCompat.checkSelfPermission(recyclerView.getContext(), android.Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_CONTACTS},
                    0);
        }

        if(recyclerView != null) {
            adapter = new GroupAdapter();
            recyclerView.setAdapter(adapter);
            groupData.observe(this, adapter);
            recyclerView.invalidate();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater mInflater = getMenuInflater();
        mInflater.inflate(R.menu.group_list_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.logout_button:
                SharedPreferences.Editor editor = getSharedPreferences(LoginActivity.SHAREDPREFFILE,
                        Context.MODE_PRIVATE).edit();
                editor.remove(OAuthService.USERIDPREF);
                editor.remove(OAuthService.TOKENPREF);
                editor.apply();
                Intent outIntent = new Intent(this, LoginActivity.class);
                startActivity(outIntent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.d("onNewIntent", intent.getStringExtra("type"));

        if(intent.getStringExtra("type").equals("MESSAGE_RECEIVED")) {
            Log.d("onNewIntent", intent.getStringExtra("groupId"));

            ((GroupAdapter)recyclerView.getAdapter()).openGroup(intent.getStringExtra("groupId"));
        }
    }

  /**
   * Creates a new GroupAdapter with the updated groups, or instantiates the recyclerview.
   *
   */
//  public void onGroups(List<Group> _) {
//      for (Group group : Session.groups) {
//          Session.getMembers(group.getId());
//      }
//    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        Log.d("NOTIFICATION", "check play services");
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.d("GROUPLISTACTIVITY", "This device is not supported by Google Play Services.");
                //ToastNotify("This device is not supported by Google Play Services.");
                finish();
            }
            return false;
        }
        return true;
    }

    public void registerWithNotificationHubs()
    {
        Log.d("NOTIFICATION", "register with hub");
        if (checkPlayServices()) {
            // Start IntentService to register this application with FCM.
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        isVisible = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        isVisible = false;
    }

    @Override
    protected void onStop() {
        super.onStop();
        //groupData.removeObserver((GroupAdapter) recyclerView.getAdapter());
        isVisible = false;
    }

    @Override
    public LifecycleRegistry getLifecycle() {
        return mRegistry;
    }


    /**
     * The adapter for the group list recyclerview.
     */
    public class GroupAdapter
            extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Observer<Group[]> {

        private Group[] groups;

        public GroupAdapter() {
            groups = new Group[0];
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.group_list_content, parent, false);
            return new GroupViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
            final Group group = groups[position];
            final GroupViewHolder groupViewHolder = (GroupViewHolder) holder;

            // Bind the group to the view holder.
            groupViewHolder.group = group;
            // Set the text of the display id to be the group's display name.
            groupViewHolder.idText.setText(group.getName());

            if (group.hasMembers()) {
                groupViewHolder.memberText.setText(group.getMembers());
            }
            // Set a listener to open the group message history when clicked.
            groupViewHolder.view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //set the previous selected groups text to black
                    if (previousSelectedGroup != null) {
                        previousSelectedGroup.idText.setTextColor(Color.BLACK);
                        previousSelectedGroup.activeRectangle.clearColorFilter();
                    }
                    //keep track of the last group clicked on to color the selected groups name
                    previousSelectedGroup = (GroupViewHolder) holder;
                    //set the selected groups text to green
                    groupViewHolder.idText.setTextColor(sanitize(R.color.stylo_green));
                    groupViewHolder.activeRectangle.setColorFilter(sanitize(R.color.stylo_accent));
                    //set the current group to the group selected
                    Session.currentGroup = groupViewHolder.group.getId();

                    if (mTwoPane) {
                        Bundle arguments = new Bundle();
                        arguments.putString(GroupDetailFragment.ARG_ITEM_ID, groupViewHolder.group.getId() + "");

                        GroupDetailFragment fragment = new GroupDetailFragment();
                        fragment.setArguments(arguments);
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.group_detail_container, fragment)
                                .commit();
                    } else {
                        Context context = v.getContext();
                        Intent intent = new Intent(context, GroupDetailActivity.class);
                        intent.putExtra(GroupDetailFragment.ARG_ITEM_ID, groupViewHolder.group.getId());
                        intent.putExtra(GroupDetailFragment.ARG_ITEM_NAME, groupViewHolder.group.getName());

                        context.startActivity(intent);
                    }
                }
            });

        }

        @Override
        public int getItemCount() {
            return groups.length;
        }

        @Override
        public void onChanged(@Nullable Group[] newGroups) {
            Log.d("Recycler View", "View is changed");

            if (newGroups != null && groups.length != newGroups.length) {
                AppDAO.initialize(getApplicationContext());
                boolean result = true;
                for (int ind = 0; ind < newGroups.length && result; ind++) {
                    result = AppDAO.getMembers(newGroups[ind].getId());
                }
                if (!result) {
                    Toast.makeText(getApplicationContext(),"No internet connection. Could not get group member lists.",
                            Toast.LENGTH_SHORT).show();
                }
            }

            groups = newGroups;


            notifyDataSetChanged();
        }

        public void openGroup(String groupID) {
            for (int ind = 0; ind < groups.length; ind++) {
                if (groups[ind].getId().equals(groupID)) {
                    recyclerView.findViewHolderForAdapterPosition(ind).itemView.performClick();
                }
            }
        }


        /**
         * The model bound to a group in the {GroupAdapter}.
         */
        class GroupViewHolder extends RecyclerView.ViewHolder {
            public final View view;
            private final TextView idText;
            private final TextView memberText;
            private final ImageView activeRectangle;
            public Group group;

            GroupViewHolder(View view) {
                super(view);
                this.view = view;
                memberText = (TextView) view.findViewById(R.id.membersText);
                idText = (TextView) view.findViewById(R.id.id);
                activeRectangle = (ImageView) view.findViewById(R.id.active_rectangle);
            }
        }
    }

    @Override
    public void onChanged(@Nullable User[] groupMems) {
        for (int ind = 0; ind < adapter.groups.length; ind++) {
            for (User usr : groupMems) {
                adapter.groups[ind].addMember(usr.toString());
            }
        }

    }

    /**
     * Does a compatibility check on a color we have in our theme.
     *
     * @param color
     * @return the resource color value.
     */
    private int sanitize(int color) {
        return ContextCompat.getColor(getApplicationContext(), R.color.stylo_green);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 0: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted!
                    Log.d("Contacts Permissions", "Gave permission");
                } else {
                    // permission denied
                    Log.d("Contacts Permissions", "Did not give permission");
                }
                return;
            }
        }
    }

}
