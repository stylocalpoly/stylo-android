package com.calpoly.stylo.settings;

/**
 * Created by Jenna on 2/3/17.
 */

public class GroupSettings {

    /**
     * Changing the display name of the group
     */
    public void renameGroup(){
        //TODO: implement group renaming
    }

    /**
     *  Removing the current user from the group
     */
    public void leaveGroup(){
        //TODO: implement removing current user from the group
    }
}
