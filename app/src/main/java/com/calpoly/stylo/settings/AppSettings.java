package com.calpoly.stylo.settings;

/**
 * Created by Jenna on 2/3/17.
 */

public class AppSettings {

    private com.calpoly.stylo.group.User currentUser;

    /**
     *  Allow the app to send push notifications to the user
     */
    public void enableNotifications(){
        //TODO: implement enabling notifications
    }

    /**
     *  User will not have to log in between app launches
     */
    public void stayLoggedIn(){
        //TODO: implement staying logged in
    }

    /**
     *  Allow user to log out of their account
     */
    public void logOut(){
        //TODO: implement logging out
    }
}
