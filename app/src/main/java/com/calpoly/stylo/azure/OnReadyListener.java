package com.calpoly.stylo.azure;

/**
 * Created by andrewpeterson on 2/16/17.
 */

public interface OnReadyListener {
  void onSessionInitialized();
}
