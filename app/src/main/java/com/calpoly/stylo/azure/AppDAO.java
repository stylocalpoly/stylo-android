package com.calpoly.stylo.azure;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.db.SupportSQLiteOpenHelper;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.calpoly.stylo.LoginActivity;
import com.calpoly.stylo.group.CurrentUser;
import com.calpoly.stylo.group.Group;
import com.calpoly.stylo.group.User;
import com.google.common.util.concurrent.CycleDetectingLockFactory;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by Zack Cody & Ryan Kehlenbeck on 5/25/2017.
 */

public class AppDAO {
    private static OfflineDatabase dataBase;
    private static OfflineDAO offlineDAO;
    private static Context context;
    private static Blob blob;

    @Database(entities = {Group.class, CurrentUser.class, Message.class}, version = 8)
    public static abstract class OfflineDatabase extends RoomDatabase {
        public abstract OfflineDAO offlineDAO();
    }

    public static LiveData<Group[]> getGroups() {
        Log.d("Network", isNetworkAvailable() + "");
        Log.d("AppDAO + GetGroups", dataBase.isOpen() + "");
        if (isNetworkAvailable()) {
            Futures.addCallback(Session.getGroups(), new FutureCallback<JsonElement>() {
                @Override
                public void onSuccess(JsonElement element) {
                    Log.d("Callback", "Success");
                    // Create the type list of Group
                    Type listType = new TypeToken<List<Group>>() {
                    }.getType();
                    // Convert the JsonElement into Group POJOs
                    Gson gson = new Gson();
                    final List<Group> userGroups = gson.fromJson(element, listType);
                    AsyncTask insertGroups = new AsyncTask() {
                        @Override
                        protected Object doInBackground(Object[] objects) {
                            offlineDAO.insertGroups(userGroups);
                            Log.d("Insertion task", "Done inserting groups");
                            return null;
                        }
                    };
                    Log.d("AppDAO", "Inserting groups");
                    insertGroups.execute();
                }

                @Override
                public void onFailure(Throwable throwable) {
                    throwable.printStackTrace();
                }
            });
        }
        else {
            Toast.makeText(context,"No Internet Connection. Could not download groups.",
                    Toast.LENGTH_SHORT).show();
        }

        return offlineDAO.getGroups();
    }

    public static boolean getMembers(final String group) {
        if (isNetworkAvailable()) {
            Futures.addCallback(Session.getMembers(group), new FutureCallback<JsonElement>() {
                @Override
                public void onSuccess(JsonElement response) {
                    // Create the type list of Group
                    Type listType = new TypeToken<List<String>>() {
                    }.getType();
                    // Convert the JsonElement into Group POJOs
                    Gson gson = new Gson();
                    List<String> members = gson.fromJson(response, listType);
                    String mems = "";

                    for (String member : members) {
                        mems = mems + ", " + member;
                    }
                    mems = mems.replaceFirst(", ", "");
                    final String memList = mems;

                    AsyncTask insertMems = new AsyncTask() {
                        @Override
                        protected Object doInBackground(Object[] objects) {
                            offlineDAO.insertMembers(memList, group);
                            Log.d("Insertion task", "Done inserting memberss");
                            return null;
                        }
                    };
                    Log.d("AppDAO", "Inserting members");
                    insertMems.execute();
                }

                @Override
                public void onFailure(Throwable throwable) {
                    throwable.printStackTrace();
                }
            });
            return true;
        }
        return false;
    }

    public static LiveData<Message[]> getMessages(final File direct, final String group) {
        if (isNetworkAvailable()) {
            startBlob();
            AsyncTask insertMessages = new AsyncTask() {
                @Override
                protected Object doInBackground(Object[] objects) {
                    try {
                        List<Message> msgs = blob.getMessages(direct, group);
                        offlineDAO.insertMessages(msgs);
                        Log.d("Insertion task", "Done inserting messages");
                    } catch (final Exception e) {
                        e.printStackTrace();
                    }
                    return null;
                }
            };
            Log.d("AppDAO", "Inserting groups");
            insertMessages.execute();
        }
        else {
            Toast.makeText(context,"No Internet Connection. Could not download messages.",
                    Toast.LENGTH_SHORT).show();
        }

        return offlineDAO.getMessages(group);
    }

    public static void getMessage(final File direct, final String mId, final String group) {
        if (isNetworkAvailable()) {
            startBlob();
            AsyncTask insertMessage = new AsyncTask() {
                @Override
                protected Object doInBackground(Object[] objects) {
                    try {
                        Message newMsg = blob.getMessage(direct, mId, group);
                        offlineDAO.insertMessage(newMsg);
                        Log.d("Insertion task", "Done inserting new message");
                    } catch (final Exception e) {
                        e.printStackTrace();
                    }
                    return null;
                }
            };
            insertMessage.execute();
        }
        else {
            Toast.makeText(context,"No Internet Connection. Could not download new message.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    public static void getCurrentUser(FutureCallback<User> callback) {
        final FutureCallback<User> userCallback = callback;
        AsyncTask<Void, Void, CurrentUser[]> getUserTask = new AsyncTask<Void, Void, CurrentUser[]>() {
            @Override
            protected CurrentUser[] doInBackground(Void... params) {
                return offlineDAO.getCurrentUser();
            }

            @Override
            protected void onPostExecute(CurrentUser[] tempUser) {
                if (tempUser.length > 0) {
                    userCallback.onSuccess(new User(tempUser[0].getId(), tempUser[0].getFirstName(), tempUser[0].getLastName(), tempUser[0].getEmail()));
                }
                else {
                    userCallback.onFailure(new Throwable(LoginActivity.GET_USER_FAIL));
                }
            }
        };
        getUserTask.execute();
    }

    public static void addMessage(final Message msg) {
        final Message mess = msg;
        AsyncTask<Void, Void, Void> insMsgTask = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                offlineDAO.insertMessage(mess);
                return null;
            }
        };
        insMsgTask.execute();
    }

    public static void setCurrentUser(CurrentUser usr, final FutureCallback callback) {
        final CurrentUser curUser = usr;
        final FutureCallback setCallback = callback;
        AsyncTask setUserTask = new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] params) {
                offlineDAO.deleteCurrentUser();
                offlineDAO.setCurrentUser(curUser);
                return null;
            }

            @Override
            protected void onPostExecute(Object obj) {
                callback.onSuccess(null);
            }
        };
        setUserTask.execute();
    }

    public static void clearTables() {
        AsyncTask<Void, Void, Void> clearTask = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                offlineDAO.deleteGroups();
                offlineDAO.deleteMessages();
                for (File fi : context.getFilesDir().listFiles()) {
                    if (!fi.getName().equals("drafts")) {
                        if (fi.isDirectory()) {
                            for (File infi : fi.listFiles()) {
                                infi.delete();
                            }
                        }
                        fi.delete();
                    }
                }
                return null;
            }
        };
        clearTask.execute();
    }


    public static void updateMessagesSent() {
            Log.d("OFFLINE", "In update messages method");
            startBlob();
            AsyncTask<Void, Void, Boolean> getMessagesTask = new AsyncTask<Void, Void, Boolean>() {
                @Override
                protected Boolean doInBackground(Void... params) {
                    Log.d("OFFLINE", "Running the task");
                    Message[] messages = offlineDAO.getUnsentMessages(0);
                    if (messages != null && messages.length > 0) {
                        Log.d("OFFLINE", "Number of unset messages = " + messages.length);
                        for (Message message : messages) {
                            try {
                                Log.d("OFFLINE FILE UPDATE", message.getSource().toString());
                                Session.pushMessage(blob.sendMessage(message.getSource(), message.getMetadata()));
                                message.setIsSent(1);
                                offlineDAO.insertMessage(message);
                            }catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }
                    }
                    return messages != null && messages.length > 0;
                }
                @Override
                protected void onPostExecute(Boolean result) {
                    if (result) {
                        Toast.makeText(context,"Successfully sent unsent messages!",
                                Toast.LENGTH_LONG).show();
                    }
                }
            };
            getMessagesTask.execute();
    }

    public static void sendMessage(Message newMessage) {
        final Message finalMessage = newMessage;
        if (isNetworkAvailable()) {
            startBlob();
            AsyncTask<Void, Void, Void> sendMessageTask = new AsyncTask<Void, Void, Void>(){
                @Override
                protected Void doInBackground(Void... params) {
                    try {
                        Log.d("OFFLINE FILE SAVE", finalMessage.getSource().toString());
                        Session.pushMessage(blob.sendMessage(finalMessage.getSource(), finalMessage.getMetadata()));
                    } catch (final Exception e) {
                        e.printStackTrace();
                    }
                    return null;
                }
            };
            sendMessageTask.execute();
        }
        else {
            newMessage.setIsSent(0);
            Toast.makeText(context,"No Internet Connection. Could not send message now.",
                    Toast.LENGTH_LONG).show();
        }
        final Message cacheMessage = newMessage;
        AsyncTask<Void, Void, Void> insertMessageTask = new AsyncTask<Void, Void, Void>(){
            @Override
            protected Void doInBackground(Void... params) {
                Log.d("OFFLINE Insert message", cacheMessage.getSource().toString());
                Log.d("CACHE", "exists: " + cacheMessage.getSource().exists());
                offlineDAO.insertMessage(cacheMessage);
                return null;
            }
        };
        insertMessageTask.execute();
    }

    public static boolean isNetworkAvailable() {

        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        boolean isAvalibale = activeNetworkInfo != null && activeNetworkInfo.isConnected();
        if (isAvalibale) {
            updateMessagesSent();
        }
        return isAvalibale;
    }

    public static void initialize(Context firstContext) {
        if (dataBase == null) {
            Log.d("DB", "Before DB build");
            dataBase = Room.databaseBuilder(firstContext.getApplicationContext(),
                    OfflineDatabase.class, "offline-database").build();
            Log.d("DB", "After DB build");
            offlineDAO = dataBase.offlineDAO();
            context = firstContext.getApplicationContext();
            Log.d("AppDAO + Initialize", dataBase.isOpen() + "");
        }
    }

    private static void startBlob() {
        if (blob == null) {
            blob = new Blob();
        }
    }

    public static boolean isOpen() {
        return dataBase.isOpen();
    }
}
