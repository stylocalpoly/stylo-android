package com.calpoly.stylo.azure;

import android.app.Activity;
import android.content.SharedPreferences;

import com.google.common.util.concurrent.ListenableFuture;
import com.microsoft.windowsazure.mobileservices.MobileServiceClient;
import com.microsoft.windowsazure.mobileservices.authentication.MobileServiceAuthenticationProvider;
import com.microsoft.windowsazure.mobileservices.authentication.MobileServiceUser;

import java.net.MalformedURLException;

/**
 * Created by andrewpeterson on 1/30/17.
 */

public class OAuthService {

    public static final String USERIDPREF = "uid";
    public static final String TOKENPREF = "tkn";
    private final String appUrl = "https://penmessageapp.azurewebsites.net";

    private SharedPreferences prefs;
    public Activity activity;
    public MobileServiceClient client;

    public OAuthService(SharedPreferences preferences, Activity activity) {
        this.prefs = preferences;
        this.activity = activity;

        try {
            client = new MobileServiceClient(appUrl, activity);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    public boolean hasToken() {
        return prefs.getString(USERIDPREF, null) != null
                && prefs.getString(TOKENPREF, null) != null;
    }

    public void loadToken() {
        String userId = prefs.getString(USERIDPREF, null);
        String token = prefs.getString(TOKENPREF, null);

        MobileServiceUser user = new MobileServiceUser(userId);
        user.setAuthenticationToken(token);
        client.setCurrentUser(user);
        Session.msUser = user;
    }

    public void cacheUser(MobileServiceUser user) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(USERIDPREF, user.getUserId());
        editor.putString(TOKENPREF, user.getAuthenticationToken());
        editor.commit();
    }
}
