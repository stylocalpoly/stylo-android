package com.calpoly.stylo.azure;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverter;
import android.arch.persistence.room.TypeConverters;
import android.graphics.Bitmap;
import android.util.Log;
import android.util.Pair;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.sql.Date;
import java.util.Calendar;
import java.util.Comparator;
import java.util.HashMap;

/**
 * Created by andrewpeterson on 3/3/17.
 */

@Entity
@TypeConverters(Message.MessageConverters.class)
public class Message implements Comparable<Message> {
    @PrimaryKey
    public String name;
    public String username;
    public String groupId;
    public HashMap<String, String> metadata;
    public Date timestamp;
    public File source;
    public int isSent;

    @Ignore
    public Message(File source, HashMap<String, String> metadata, String grp) {
        this.source = source;
        this.metadata = metadata;
        this.name = source.getName();
        this.groupId = grp;
        this.timestamp = new Date(Long.parseLong(metadata.get("timestamp")) * 1000);
        this.isSent = 1;

        if (metadata.containsKey("username")) {
            username = metadata.get("username");
        }
        // Java 8? Can we plz haz?
        // this.timestamp = Date.from(Instant.ofEpochSecond(metadata.get("timestamp")));
    }

    public Message() {
        name = "";
        username = "";
        groupId = "";
        isSent = 1;
    }

    @Override
    public int compareTo(Message other) {
        return timestamp.compareTo(other.timestamp);
    }


    @Override
    public boolean equals(Object other) {
        if (other instanceof Message) {
            if (((Message)other).getName().equals(name)) {
                return true;
            }
        }
        return false;
    }

    public String getName() {
        return name;
    }
    public String getUsername() {
        return username;
    }
    public HashMap<String, String> getMetadata() {
        return metadata;
    }
    public Date getTimestamp() {
        return timestamp;
    }
    public File getSource() {
        return source;
    }
    public String getGroupId() { return groupId;}
    public int getIsSent() { return isSent;}

    public void setName(String newName) {
        name = newName;
    }
    public void setUsername(String newName) {
        username = newName;
    }
    public void setMetadata(HashMap<String,String> meta) {
        metadata = meta;
    }
    public void setTimestamp(Date time) {
        timestamp = time;
    }
    public void setSource(File src) {
        source = src;
    }
    public void setGroupId(String grp) { groupId = grp;}
    public void setIsSent(int sent) { isSent = sent;}

    public static class MessageConverters {
        @TypeConverter
        public Date fromTimestamp(Long value) {
            return value == null ? null : new Date(value);
        }

        @TypeConverter
        public Long dateToTimestamp(Date date) {
            if (date == null) {
                return null;
            } else {
                return date.getTime();
            }
        }

        @TypeConverter
        public File fromString(String nFile) {
            return new File(nFile);
        }

        @TypeConverter
        public String filetoString(File nFile) {
            return nFile.getPath();
        }

        @TypeConverter
        public HashMap<String, String> fromStringMap(String map) {
            HashMap<String, String> meta = new HashMap<String,String>();
            Gson gson = new Gson();
            try {
                JSONObject obj = gson.fromJson(map, JSONObject.class);
                JSONArray nms = obj.names();
                for (int ind = 0; ind < nms.length(); ind++) {
                    meta.put(nms.getString(ind), obj.getString(nms.getString(ind)));
                }
            }
            catch (Exception ex) {
                Log.d("Message", "Couldn't convert metadata");
                ex.printStackTrace();
            }
            return meta;
        }

        @TypeConverter
        public String hashtoString(HashMap<String, String> hash) {
            JSONObject obj = new JSONObject();
            try {
                for (String key : hash.keySet()) {
                    obj.put(key, hash.get(key));
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            Gson gson = new Gson();
            return gson.toJson(obj);
        }
    }

}
