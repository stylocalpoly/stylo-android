package com.calpoly.stylo.azure;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.util.Pair;
import android.widget.Toast;

import com.calpoly.stylo.LoginActivity;
import com.calpoly.stylo.group.Group;
import com.calpoly.stylo.group.User;
import com.google.common.util.concurrent.AsyncFunction;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.microsoft.windowsazure.mobileservices.MobileServiceClient;
import com.microsoft.windowsazure.mobileservices.authentication.MobileServiceAuthenticationProvider;
import com.microsoft.windowsazure.mobileservices.authentication.MobileServiceUser;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;


/**
 * Session is a static class that wires asyncrhonous database calls throughout the entire app.
 * It is comprised of a public API, that is deconstructed into several package-private classes,
 * such as Transform and Callbacks.
 */
public class Session {
    public static final String appUrl = "https://penmessageapp.azurewebsites.net";

    // Our connection to the Azure Cloud Services.
    public static MobileServiceClient client;
    // The Azure user, not to be confused with an internal User.
    public static MobileServiceUser msUser;
    // The user of the application.
    public static User user;
    // The members in the selected groups
    // TODO: this needs to be something other than a list of strings
    public static List<String> members = new ArrayList<>();
    // The OAuth configuration for the session.
    private static OAuthService OAuth;
    // The current active group for the message history and the Canvas
    public static String currentGroup = "5c898ce8-ee9c-4736-80bd-fc81bbe56762";

    /**
     * A static constructor for Session.
     *
     * @param OAuth the OAuth instance we are using for this session.
     */
    public static void initialize(OAuthService OAuth) {
        Session.OAuth = OAuth;
        Session.client = OAuth.client;
    }

    /**
     * Triggers two asynchronous steps:
     * Gets the Azure User ->
     * Gets the internal User
     *
     * @param provider the specific OAuth provider (Facebook Google Microsoft)
     * @return a future for when the user is loaded.
     */
    public static ListenableFuture<User> login(MobileServiceAuthenticationProvider provider) {
        // Login and
        return Futures.transform(client.login(provider), fromAzureToUser);
    }

    public static ListenableFuture<User> googleLogin() {
        msUser = client.getCurrentUser();
        OAuth.cacheUser(msUser);
        return Session.getUser();
    }

    /**
     * Gets the User object for the logged in user from the Azure User Table.
     *
     * @return the future for when the user is loaded.
     */
    public static ListenableFuture<User> getUser() {
        return client.invokeApi("getUser", "GET", null, User.class);
    }

    /**
     * Inserts a new group into the Group table and adds this user to the GroupXUSer table.
     *
     * @param group the new group POJO.
     */
    public static ListenableFuture<Group> createGroup(Group group, Context context) {
        List<Pair<String, String>> params = new ArrayList();
        params.add(new Pair("groupName", group.getName()));
        if (AppDAO.isNetworkAvailable()) {
            // Add the new group
            ListenableFuture<Group> newGroup = client.invokeApi("createGroup", "POST", params, Group.class);

            Futures.addCallback(newGroup, new FutureCallback<Group>() {
                @Override
                public void onSuccess(Group receivedGroup) {
                    Log.d("CREATING NEW GROUP", receivedGroup.toString());
                    AppDAO.getGroups();
                }

                @Override
                public void onFailure(Throwable throwable) {
                    throwable.printStackTrace();
                }
            });
            return newGroup;
        }
        else {
            Toast.makeText(context, "No internet connection. Could not add the new group. Try again later.",
                    Toast.LENGTH_SHORT).show();
        }

        return null;
    }

    // TODO: listenable future getMembers(Group group)
    public static ListenableFuture<JsonElement> getMembers(final String groupId) {
        List<Pair<String, String>> params = new ArrayList();
        params.add(new Pair("id", groupId));

        ListenableFuture<JsonElement> memberFuture = client.invokeApi("getMembers", "POST", params);

        return memberFuture;
    }

    /**
     * Makes two calls to the database, one to the GroupXUser table, then another to the Group Table.
     * The method returns a list of groups.
     *
     * @return the list of groups for the user.
     */
    public static ListenableFuture<JsonElement> getGroups() {
        // Get all the groups from Azure
        ListenableFuture<JsonElement> groupFuture = client.invokeApi("getGroups");

        return groupFuture;
    }

    public static void pushMessage(String blobName) {
        List<Pair<String, String>> params = new ArrayList();
        params.add(new Pair("groupId", Session.currentGroup));
        params.add(new Pair("messageId", blobName));

        client.invokeApi("pushMessage", "POST", params, String.class);
    }
    
    public static ListenableFuture<JsonElement> addUserToGroup(String groupId, String email, Context context) {
        List<Pair<String, String>> params = new ArrayList();
        params.add(new Pair("groupId", groupId));
        params.add(new Pair("email", email));

        if (AppDAO.isNetworkAvailable()) {
            // Add the new group
            // change this response type to int
            ListenableFuture<JsonElement> groupFuture = client.invokeApi("addUser", "POST", params);

            Futures.addCallback(groupFuture, new FutureCallback<JsonElement>() {
                @Override
                public void onSuccess(JsonElement response) {
                    Log.d("addUserToGroup", "Successfully added the user");
                }

                @Override
                public void onFailure(Throwable throwable) {
                    throwable.printStackTrace();
                }
            });

            return groupFuture;
        }
        else {
            Toast.makeText(context, "No internet connection. Could not add user to group. Try again later.", Toast.LENGTH_SHORT).show();
        }
        return null;
    }

    static AsyncFunction<MobileServiceUser, User> fromAzureToUser = new AsyncFunction<MobileServiceUser, User>() {
        @Override
        public ListenableFuture<User> apply(MobileServiceUser mobileServiceUser) throws Exception {
            msUser = mobileServiceUser;
            OAuth.cacheUser(msUser);
            return Session.getUser();
        }
    };
}
