package com.calpoly.stylo.azure;

import android.util.Log;

import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.blob.BlobListingDetails;
import com.microsoft.azure.storage.blob.CloudBlob;
import com.microsoft.azure.storage.blob.CloudBlobClient;
import com.microsoft.azure.storage.blob.CloudBlobContainer;
import com.microsoft.azure.storage.blob.CloudBlockBlob;
import com.microsoft.azure.storage.blob.ListBlobItem;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * This class handles communication with Azure Blob storage.
 */
public class Blob {

    // The string for connecting to Azure Cloud Storage.
    private static String storageConnectionString =
            "DefaultEndpointsProtocol=http;" +
                    "AccountName=penmessagestorage;" +
                    "AccountKey=BV5WR1Km404XR6K8F/KxOKuAyTw0utckHVZvOqW/LO5+cUTNVdZ9hShhBS/oOR7VAjKaSlt9+nBVVLXdvRpCgQ==";


    // The connection client we use to interface with Azure Blob Storage.
    private CloudBlobClient client;


    public Blob() {
        try {
            CloudStorageAccount storageAccount = CloudStorageAccount.parse(storageConnectionString);
            // Create the blob client.
            client = storageAccount.createCloudBlobClient();

        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    public List<Message> getMessages(File directory, String group) {
      List<Message> messages = new ArrayList<>();

      try {

        directory.mkdir();

        // Retrieve reference to a previously created container.
        CloudBlobContainer container = client.getContainerReference(group);
        // Loop through each blob item in the container.
        for (ListBlobItem blobItem : container.listBlobs()) {
          // If the item is a blob, not a virtual directory.
          if (blobItem instanceof CloudBlockBlob) {
            // Download the item and save it to a message with the same name.
            CloudBlockBlob blob = (CloudBlockBlob) blobItem;
            blob.downloadAttributes();

            File source = new File(directory, blob.getName());
            messages.add(new Message(source, blob.getMetadata(), group));
            blob.download(new FileOutputStream(source));

          }
        }
      } catch (Exception e) {
        // Output the stack trace.
        e.printStackTrace();
      }

      Collections.sort(messages);

      return messages;

    }


  public Message getMessage(File directory, String messageId, String group) {

    try {
      directory.mkdir();

      // Retrieve reference to a previously created container.
      CloudBlobContainer container = client.getContainerReference(group);

      CloudBlockBlob blob = container.getBlockBlobReference(messageId);
      // Loop through each blob item in the container.

      // Download the item and save it to a message with the same name.
      blob.downloadAttributes();

      File source = new File(directory,  blob.getName());
      blob.download(new FileOutputStream(source));
      Log.d("getMessage", blob.getName());
      return new Message(source, blob.getMetadata(), group);
    }
    catch (Exception e) {
      e.printStackTrace();
    }

    return null;
  }

    public String sendMessage(File source, HashMap<String, String> metadata) {
        String reference = makeName(metadata);

        try {
            CloudBlobContainer container = client.getContainerReference(Session.currentGroup);
            container.createIfNotExists();

            CloudBlockBlob blob =
                    container.getBlockBlobReference(reference);

            blob.setMetadata(metadata);

            Log.d(Blob.class.getName(), "sendMessage -> metadata: " + blob.getMetadata());

            blob.upload(new FileInputStream(source), source.length());
            blob.uploadMetadata();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return reference;
    }

    public static String makeName(HashMap<String, String> meta) {
        return meta.get("sender") + meta.get("timestamp") + ".will";
    }
}
