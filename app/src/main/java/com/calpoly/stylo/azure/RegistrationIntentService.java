package com.calpoly.stylo.azure;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.calpoly.stylo.GroupListActivity;
import com.google.firebase.iid.FirebaseInstanceId;
import com.microsoft.windowsazure.messaging.NotificationHub;

/**
 * Created by roslynsierra on 3/10/17.
 */
public class RegistrationIntentService extends IntentService {

    private static final String TAG = "RegIntentService";

    private NotificationHub hub;

    public RegistrationIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String resultString = "";
        String regID;
        //Test smart commit
        try {
            String FCM_token = FirebaseInstanceId.getInstance().getToken();
            Log.d(TAG, "FCM Registration Token: " + FCM_token);

            NotificationHub hub = new NotificationHub(NotificationSettings.HubName,
                    NotificationSettings.HubListenConnectionString, this);

            regID = hub.register(FCM_token, Session.client.getCurrentUser().getUserId()).getRegistrationId();

            sharedPreferences.edit().putString("registrationID", regID ).apply();
            sharedPreferences.edit().putString("FCMtoken", FCM_token ).apply();
        } catch (Exception e) {
            Log.e(TAG, resultString="Failed to complete registration", e);
            // If an exception happens while fetching the new token or updating our registration data
            // on a third-party server, this ensures that we'll attempt the update at a later time.
        }
    }
}