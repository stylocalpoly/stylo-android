package com.calpoly.stylo.azure;

import android.support.v7.widget.RecyclerView;

import com.calpoly.stylo.group.Group;

import java.util.List;

/**
 * Created by Jenna on 5/15/17.
 */

public interface OnMembers {
    public void onMembers(String groupId, List<String> members);
}
