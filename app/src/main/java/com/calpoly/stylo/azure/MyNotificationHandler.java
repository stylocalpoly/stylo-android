package com.calpoly.stylo.azure;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v7.view.ContextThemeWrapper;
import android.util.Log;

import com.calpoly.stylo.GroupDetailActivity;
import com.calpoly.stylo.GroupDetailFragment;
import com.calpoly.stylo.GroupListActivity;
import com.calpoly.stylo.LoginActivity;
import com.calpoly.stylo.R;
import com.calpoly.stylo.group.Group;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.firebase.messaging.RemoteMessage;
import com.microsoft.windowsazure.notifications.NotificationsHandler;

import java.io.File;
import java.security.acl.NotOwnerException;

/**
 * Created by roslynsierra on 3/9/17.
 */

public class MyNotificationHandler extends NotificationsHandler {
    public static final int NOTIFICATION_ID = 1;
    private NotificationManager mNotificationManager;
    NotificationCompat.Builder builder;
    Context context;

    @Override
    public void onReceive(Context context, Bundle bundle) {
        Log.d("Notification", "received.");
        Log.d("onReceive", "notification for " + bundle.getString("type"));

        this.context = context;

        String type = bundle.getString("type");

        if (type.equals("ADDED_TO_GROUP")
                && Session.user.getId().equals(bundle.getString("id"))) {
            Log.d("onReceive", "refreshing this user's groups.");
            AppDAO.initialize(context);
            AppDAO.getGroups();
            createGroupNotification(bundle.getString("name"));
        }

        if(type.equals("MESSAGE_RECEIVED")) {
            String groupId = bundle.getString("groupId");
            String messageId = bundle.getString("messageId");
            Log.d("onRecieve", "messageId => " + messageId);

            if(GroupListActivity.isVisible) {
                Log.d("onRecieve", "the group list activity is visible.");
            }

            if(GroupDetailActivity.isVisible) {
                Log.d("onRecieve", "the group detail activity is visible.");
            }


            // userID check goes here
            // If the group list activity is visible, and we have the group selected, just refresh messages, not notify.
            if((GroupListActivity.isVisible || GroupDetailActivity.isVisible)
                    && Session.currentGroup != null
                    && Session.currentGroup.equals(groupId)) {
                File directory = context.getDir(groupId, Context.MODE_PRIVATE);
                AppDAO.initialize(context);
                AppDAO.getMessage(directory,messageId, groupId);
            } else if(shouldNotify(groupId)) {
                createMessageNotification(groupId);
            }
        }
    }

  /**
   * Decides if a user should be notified of a push to a group.
   * @param groupId
   * @return
   */
  public boolean shouldNotify(String groupId) {
        boolean shouldNotify = false;
        Group[] groups = AppDAO.getGroups().getValue();
        if (groups != null) {
            for (Group group : groups) {
                if (group.getId().equals(groupId)) {
                    shouldNotify = true;
                }
            }
        }

        return shouldNotify;
    }

    public void createMessageNotification(String groupId) {

        Intent intent = GroupListActivity.mTwoPane ?
                new Intent(context, GroupListActivity.class)
                        .putExtra("type", "MESSAGE_RECEIVED")
                        .putExtra("groupId", groupId)
                        .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                : new Intent(context, GroupDetailActivity.class)
                    .putExtra(GroupDetailFragment.ARG_ITEM_ID, groupId + "")
                    .putExtra(GroupDetailFragment.ARG_ITEM_NAME, "New Group");

        PendingIntent contentIntent = PendingIntent.getActivity(context,
                0, // requestCode
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        Notification notification = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.ic_send_white_24dp)
                .setContentTitle("New Message")
                .setStyle(new NotificationCompat.BigTextStyle().bigText("You got a new message!"))
                .setContentText("You got a new message!")
                .setContentIntent(contentIntent)
                .setAutoCancel(true)
                .setPriority(Notification.PRIORITY_HIGH)
                .setVibrate(new long[0])
                .build();

        NotificationManager notificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(NOTIFICATION_ID, notification);
    }


    public void createGroupNotification(String name) {
        PendingIntent contentIntent = PendingIntent.getActivity(context,
                0, // requestCode
                new Intent(context, GroupListActivity.class)
                        .putExtra("type", "ADDED_TO_GROUP")
                        .putExtra("name", name)
                        .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP),
                PendingIntent.FLAG_UPDATE_CURRENT); // flags

        Notification notification = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.ic_send_white_24dp)
                .setContentTitle("New Group")
                .setStyle(new NotificationCompat.BigTextStyle().bigText("You have been added to " + name + "."))
                .setContentText("You have been added to " + name + ".")
                .setContentIntent(contentIntent)
                .setAutoCancel(true)
                .setPriority(Notification.PRIORITY_HIGH)
                .setVibrate(new long[0])
                .build();

        NotificationManager notificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(NOTIFICATION_ID, notification);
    }
}