package com.calpoly.stylo.azure;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.util.StringUtil;

import com.calpoly.stylo.group.CurrentUser;
import com.calpoly.stylo.group.Group;

import java.util.List;

/**
 * Created by Zack Cody & Ryan Kehlenbeck on 5/25/2017.
 */

@Dao
public interface OfflineDAO {
    @Query("SELECT * from GroupTable")
    public LiveData<Group[]> getGroups();

    @Query("SELECT * from CurrentUser")
    public CurrentUser[] getCurrentUser();

    @Query("DELETE from CurrentUser")
    public void deleteCurrentUser();

    @Query("SELECT * from Message where groupId = :groupId")
    public LiveData<Message[]> getMessages(String groupId);

    @Query("SELECT * from Message where isSent = :isSent")
    public Message[] getUnsentMessages(int isSent);

    @Query("UPDATE GroupTable SET members = :members where id = :groupId")
    public void insertMembers(String members, String groupId);

    @Query("DELETE from GroupTable")
    public void deleteGroups();

    @Query("DELETE from Message")
    public void deleteMessages();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void setCurrentUser(CurrentUser usr);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insertGroups(List<Group> groups);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insertMessages(List<Message> msgs);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insertMessage(Message msg);
}
