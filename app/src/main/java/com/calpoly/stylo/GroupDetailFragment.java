package com.calpoly.stylo;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.arch.lifecycle.LifecycleFragment;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.calpoly.stylo.azure.AppDAO;
import com.calpoly.stylo.azure.Message;
import com.calpoly.stylo.document.DocumentManager;
import com.calpoly.stylo.azure.Session;
import com.calpoly.stylo.message.MessageViewHolder;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * A fragment representing a single Group detail screen.
 * This fragment is either contained in a {@link GroupListActivity}
 * in two-pane mode (on tablets) or a {@link GroupDetailActivity}
 * on handsets.
 */
public class GroupDetailFragment extends LifecycleFragment {

    // The fragment argument key.
    public static final String ARG_ITEM_ID = "item_id";
    public static final String ARG_ITEM_NAME = "item_name";
    // The permissions constant for the callback method
    // The group this fragment represents.
    private String groupName = "";
    private String groupID = "-1";
    // The location of the storage directory in the cache. The group ID is contained here.
    public File location;
    // The messages view.
    public RecyclerView recyclerView;
    // Access to the activity that the fragment is in
    private Activity detailActivity;
    // Text input field that users add members from
    private EditText memberEmailInput;
    // If contacts permissions have been enabled
    private boolean showContacts = false;

    private static LiveData<Message[]> msgData;

    /*
     * Defines an array that contains column names to move from
     * the Cursor to the ListView.
     */
    @SuppressLint("InlinedApi")
    private final static String[] FROM_COLUMNS = {
            ContactsContract.CommonDataKinds.Email.DISPLAY_NAME_PRIMARY
    };

    @SuppressLint("InlinedApi")
    private static final String[] PROJECTION =
            {
                    ContactsContract.CommonDataKinds.Email._ID,
                    ContactsContract.CommonDataKinds.Email.DISPLAY_NAME_PRIMARY,
                    ContactsContract.CommonDataKinds.Email.ADDRESS,
            };

    // Defines the text expression
    private String SELECTION = "";

    // Defines a variable for the search string
    private String mSearchString = "";
    /*
     * Defines an array that contains resource ids for the layout views
     * that get the Cursor column contents. The id is pre-defined in
     * the Android framework, so it is prefaced with "android.R.id"
     */
    private final static int[] TO_IDS = {
            android.R.id.text1
    };
    // Define a ListView object
    private ListView mContactsList;
    // An adapter that binds the result Cursor to the ListView
    private SimpleCursorAdapter mCursorAdapter;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public GroupDetailFragment() {
    }

  /**
   * When created, we pull the {Group} from {Session}, and create a new directory in the cache
   * that is the name of the {Group}'s ID. Then, a new {MessageTask} is created, that asynchronously
   * pulls the messages from Blob storage and returns a list of {Message} when finished to any
   * {OnMessage} listener.
   * @param savedInstanceState
   */
  @Override
  public void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);

    Log.d("onCreate", "refreshing messages.");
      if (getArguments().containsKey(ARG_ITEM_ID)) {
          groupID = getArguments().getString(ARG_ITEM_ID);
          groupName = getArguments().getString(ARG_ITEM_NAME);

          if (msgData != null) {
              msgData = null;
          }
          getMessages(groupID);

          setupUI();
      }
  }

  private void getMessages(String group) {
      if (msgData == null) {
          location = getContext().getDir(group, Context.MODE_PRIVATE);

          AppDAO.initialize(getContext());
          msgData = AppDAO.getMessages(location, group);
      }
  }

  @Override
  public void onStart() {
    super.onStart();
  }


  /**
   * Does any kind of UI setup for the fragment.
   */
  private void setupUI() {
        Activity activity = this.getActivity();
        CollapsingToolbarLayout appBarLayout = (CollapsingToolbarLayout) activity.findViewById(R.id.toolbar_layout);
        if (appBarLayout != null) {
            appBarLayout.setTitle(groupName);
        }
    }

    private class ContactsLoader implements LoaderManager.LoaderCallbacks<Cursor> {
        @Override
        public Loader<Cursor> onCreateLoader(int loaderId, Bundle args) {
            mSearchString = memberEmailInput.getText().toString();
            SELECTION = ContactsContract.CommonDataKinds.Email.DISPLAY_NAME_PRIMARY + " LIKE '%" + mSearchString + "%' AND " + ContactsContract.CommonDataKinds.Email.ADDRESS + " LIKE '%@%'";
            Uri contentUri = ContactsContract.CommonDataKinds.Email.CONTENT_URI;
            return new CursorLoader(detailActivity.getApplicationContext(),
                    contentUri,
                    PROJECTION,
                    SELECTION,
                    null,
                    ContactsContract.CommonDataKinds.Email.DISPLAY_NAME_PRIMARY);
        }

        @Override
        public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
            // Put the result Cursor in the adapter for the ListView
            mCursorAdapter.swapCursor(cursor);
        }

        @Override
        public void onLoaderReset(Loader<Cursor> loader) {
            // Delete the reference to the existing Cursor
            mCursorAdapter.swapCursor(null);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View rootView = inflater.inflate(R.layout.group_detail, container, false);
        detailActivity = this.getActivity();

        // Add member fab
        final FloatingActionButton addMemberButton = (FloatingActionButton) rootView.findViewById(R.id.add_member_fab);
        addMemberButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(rootView.getContext());
                dialog.setContentView(R.layout.add_member_dialog);

                memberEmailInput = (EditText) dialog.findViewById(R.id.edit_text_email);
                memberEmailInput.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        if (memberEmailInput.getCurrentTextColor() == Color.RED) {
                            memberEmailInput.setTextColor(Color.BLACK);
                        }
                        if (memberEmailInput.getText().length() >= 3 && !memberEmailInput.getText().toString().contains("@") && showContacts) {
                            getLoaderManager().restartLoader(0, null, new ContactsLoader());
                        }
                        else if (memberEmailInput.getText().length() < 3 && showContacts) {
                            mSearchString = "";
                            getLoaderManager().restartLoader(0, null, new ContactsLoader());
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                    }
                });

                if (ContextCompat.checkSelfPermission(dialog.getContext(), Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
                    showContacts = true;

                    mContactsList = (ListView) dialog.findViewById(R.id.contact_list);

                    mCursorAdapter = new SimpleCursorAdapter(
                            dialog.getContext(),
                            android.R.layout.simple_list_item_1,
                            null,
                            FROM_COLUMNS,
                            TO_IDS,
                            0);
                    // Sets the adapter for the ListView
                    mContactsList.setAdapter(mCursorAdapter);
                    // Set the item click listener to be the current fragment.
                    mContactsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View item, int position, long rowID) {
                            SimpleCursorAdapter cursorAdapter = (SimpleCursorAdapter) parent.getAdapter();
                            Cursor cursor = cursorAdapter.getCursor();
                            // Move to the selected contact
                            cursor.moveToPosition(position);
                            memberEmailInput.setText(cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.ADDRESS)));
                        }
                    });
                    // Initializes the loader
                    getLoaderManager().initLoader(0, null, new ContactsLoader());
                }

                final Button ok = (Button) dialog.findViewById(R.id.finish_adding_member_button);
                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (memberEmailInput.getText().length() == 0 ||
                                !memberEmailInput.getText().toString().contains("@") ||
                                !memberEmailInput.getText().toString().contains(".")) {
                                memberEmailInput.setTextColor(Color.RED);
                            Toast.makeText(rootView.getContext(), "Please enter a valid email.", Toast.LENGTH_SHORT).show();
                        } else {
                            Session.addUserToGroup(groupID, memberEmailInput.getText().toString(), getContext());
                            dialog.dismiss();
                        }
                    }
                });



                final Button cancel = (Button) dialog.findViewById(R.id.cancel_add_member_button);
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        mSearchString = "";
                    }
                });
                dialog.show();

            }
        });

        // Open canvas fab
        final FloatingActionButton openCanvasButton = (FloatingActionButton) rootView.findViewById(R.id.open_canvas_fab);
        openCanvasButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("GROUPDETFRAG", "open canvas");
                openCanvas();
            }
        });

        // Show the dummy content as text in a TextView.
        if (!groupID.equals("-1")) {
            recyclerView = (RecyclerView) rootView.findViewById(R.id.message_container);
            final LinearLayoutManager layout = new LinearLayoutManager(getActivity());
            layout.setStackFromEnd(true);
            recyclerView.setNestedScrollingEnabled(false);
            recyclerView.setLayoutManager(layout);
            recyclerView.setAdapter(new GroupDetailFragment.MessageAdapter());
            getMessages(groupID);
            msgData.observe(this, (MessageAdapter) recyclerView.getAdapter());

            assert recyclerView != null;
        }

        return rootView;
    }

    /**
   * Serves as the mid-layer between the {Message} list and the {MessageViewHolder}.
   * This adapter does not do anything super fancy.
   */
  public class MessageAdapter
            extends RecyclerView.Adapter<MessageViewHolder> implements Observer<Message[]> {

        public Message[] messages;

        public MessageAdapter() {
            messages = new Message[0];
        }

        @Override
        public MessageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.message_content, parent, false);


            DocumentManager manager = new DocumentManager(view.getContext());
            return new MessageViewHolder(view, manager);
        }

        @Override
        public void onBindViewHolder(final MessageViewHolder holder, int position) {
            holder.message = messages[position];
        }

        @Override
        public int getItemCount() {
            return messages.length;
        }

        @Override
        public void onChanged(@Nullable Message[] newMessages) {
            Log.d("Recycler View", "Got Messages");
            messages = newMessages;
            notifyDataSetChanged();
        }
    }

    // Moves the GroupDetailFragment to the CanvasActivity.
    public void openCanvas() {
        Log.d("GROUPDETFRAG", "open canvas");
        startActivity(new Intent(this.getActivity(), CanvasActivity.class));
    }
}

