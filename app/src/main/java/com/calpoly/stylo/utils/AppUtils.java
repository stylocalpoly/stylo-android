package com.calpoly.stylo.utils;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.view.MotionEvent;

import com.wacom.ink.path.PathBuilder;
import com.wacom.ink.path.SpeedPathBuilder;

/**
 * Created by andrewpeterson on 1/30/17.
 */

public class AppUtils {
  public static final int REQUEST_CODE_IMAGE = 9999;

  /**
   * Takes a string and renders a presized bitmap.
   *
   * @param text
   * @return a Bitmap from the text.
   */
  public static Bitmap createBitmapFromString(String text) {
    // new antialised Paint
    Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
    // text color - #3D3D3D
    paint.setColor(Color.rgb(61, 61, 61));
    // text size in pixels
    paint.setTextSize((int) (14 * 10));
    // text shadow
    paint.setShadowLayer(1f, 0f, 1f, Color.WHITE);

    // draw text to the Canvas center
    Rect bounds = new Rect();
    paint.getTextBounds(text, 0, text.length(), bounds);

    Bitmap bitmap = Bitmap.createBitmap((int)paint.measureText(text), bounds.height(), Bitmap.Config.ARGB_8888);

    Canvas canvas = new Canvas(bitmap);

    // Magic function!
    canvas.drawText(text, 0, bounds.height()-bounds.bottom, paint);

    return bitmap;
  }

  /**
   * Used in various areas of the app, but determines if a PathBuilder has
   * a size large enough to create a path.
   * @param pb the pathbuilder in question.
   * @return true if it has a part size greater than 0.
   */
  public static boolean hasPartSize(PathBuilder pb) {
    return pb.getPathPartSize() > 0;
  }
}
