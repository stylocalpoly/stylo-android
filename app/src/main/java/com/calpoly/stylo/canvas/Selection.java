package com.calpoly.stylo.canvas;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;

import java.util.Stack;

/**
 * Selection contains data about currently selected items and their bounds.
 */
public class Selection {

    // The default corner node radius and side line weight
    private static final int NODE_RADIUS = 10;
    private static final int LINE_WEIGHT = 4;

    // The bounds of the selection
    private static RectF bounds;
    // The selected items
    private static Stack<CanvasElement> selected;
    // The width of the selection for computations
    private static int width;
    // The height of the selection for computations
    private static int height;

    // The corner circle image
    public static Bitmap circle;
    // The side line image
    public static Bitmap line;
    // The x position of the selection for drawing
    public static int x;
    // The y position of the selection for drawing
    public static int y;

    /**
     * Set up the selection images before drawing.
     * Must be called before other methods.
     */
    public static void initialize() {
        // Create bitmaps for the visual elements of a selection
        if (circle == null) {
            circle = Bitmap.createBitmap(NODE_RADIUS * 2, NODE_RADIUS * 2, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(circle);
            Paint paint = new Paint();
            paint.setColor(Color.GREEN);
            canvas.drawCircle(NODE_RADIUS, NODE_RADIUS, NODE_RADIUS, paint);
            line = Bitmap.createBitmap(LINE_WEIGHT, LINE_WEIGHT, Bitmap.Config.ARGB_8888);
            canvas.setBitmap(line);
            canvas.drawPaint(paint);
            selected = new Stack<>();
        }
    }

    /**
     * Calculate the bounds of the selection to draw.
     */
    public static void calculateBounds() {
        // Union the bounds of all selected elements
        bounds = new RectF(selected.get(0).getBounds());
        for (CanvasElement element : selected) {
            bounds.union(element.getBounds());
        }

        // Add the bounds of the selection itself
        width = ((int) bounds.width()) + NODE_RADIUS * 2;
        height = ((int) bounds.height()) + NODE_RADIUS * 2;
        x = ((int) bounds.left) - NODE_RADIUS;
        y = ((int) bounds.top) - NODE_RADIUS;
    }

    /**
     * Add the given element to the selection.
     *
     * @param element the element to select
     */
    public static void add(CanvasElement element) {
        selected.add(element);
    }

    /**
     * Returns whether or not any elements are selected.
     *
     * @return true if there are elements selected, false otherwise
     */
    public static boolean isSelected() {
        return !selected.isEmpty();
    }

    /**
     * Deselect any selected items.
     */
    public static void deselect() {
        selected.clear();
    }


    // Methods for computing the locations to draw selection images

    public static int circleRight() {
        return x + width - NODE_RADIUS * 2;
    }

    public static int circleBottom() {
        return y + height - NODE_RADIUS * 2;
    }

    public static int lineLeft() {
        return x + NODE_RADIUS - LINE_WEIGHT / 2;
    }

    public static int lineRight() {
        return x + width - NODE_RADIUS - LINE_WEIGHT / 2;
    }

    public static int lineTop() {
        return y + NODE_RADIUS - LINE_WEIGHT / 2;
    }

    public static int lineBottom() {
        return y + height - NODE_RADIUS - LINE_WEIGHT / 2;
    }

    public static int widthScale() {
        return (width - NODE_RADIUS) / LINE_WEIGHT;
    }

    public static int heightScale() {
        return (height - NODE_RADIUS) / LINE_WEIGHT;
    }

    public static Stack<CanvasElement> getSelected() { return selected; }

    public static RectF getBounds() { return bounds; }
}
