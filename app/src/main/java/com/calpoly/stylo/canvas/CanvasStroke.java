package com.calpoly.stylo.canvas;

import android.graphics.RectF;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.TextureView;

import com.calpoly.stylo.R;
import com.wacom.ink.path.PathBuilder;
import com.wacom.ink.rasterization.BlendMode;
import com.wacom.ink.utils.Utils;
import com.wacom.ink.willformat.Section;
import com.wacom.ink.willformat.View;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

/**
 * Created by cube on 1/23/2017.
 */

public class CanvasStroke implements CanvasElement {
    private FloatBuffer points;
    private int color;
    private int stride;
    private int size;
    private float width;
    private float startT;
    private float endT;
    private BlendMode blendMode;
    private int paintIndex;
    private int seed;
    private boolean hasRandomSeed;

    private RectF bounds;
    private FloatBuffer segmentsBounds;

    public CanvasStroke(){
        bounds = new RectF();
    }

    public CanvasStroke(int size) {
        this();
        setPoints(Utils.createNativeFloatBufferBySize(size), size);
        startT = 0.0f;
        endT = 1.0f;
    }

    public CanvasStroke(CanvasStroke stroke) {
        this.copyPoints(stroke.points, 0, stroke.size);
        this.color = stroke.color;
        this.stride = stroke.stride;
        this.size = stroke.size;
        this.width = stroke.width;
        this.startT = stroke.startT;
        this.endT = stroke.endT;
        this.blendMode = stroke.blendMode;
        this.paintIndex = stroke.paintIndex;
        this.seed = stroke.seed;
        this.hasRandomSeed = stroke.hasRandomSeed;
        this.bounds = stroke.bounds;
        this.segmentsBounds = stroke.segmentsBounds;
    }

    public void moveElement(float xpos, float ypos, int sizeOfCanvas) {
        //convert floatBuffer to float[]
        points.flip();
        float[] movedPoints = new float[ points.limit()];
        points.get(movedPoints);

        //find the difference between the initial x, y values and the new ones
        float newX = xpos - Selection.getBounds().left;
        float newY = ypos - Selection.getBounds().top;

        //x keeps track of what value we are at in the float buffer. 1 = x, 2 = y, 3 = ?
        int x = 1;
        for (int i = 0; i < movedPoints.length; i++) {
            //check if we are incrementing the x or the y value
            if (x == 1) {
                movedPoints[i] += newX;
                x++;
            } else if (x == 2){
                movedPoints[i] += newY;
                x++;
            } else {
                x = 1;
            }
        }
        //clear the point buffer and add the new moved points to the buffer
        points.clear();
        points.put(movedPoints);
    }

    public void resizeElement(boolean makeBigger, int sizeOfCanvas) {
        //convert floatBuffer to float[]
        points.flip();
        float[] movedPoints = new float[ points.limit()];
        points.get(movedPoints);

        //this is where we must move the stroke back to after spreading points out
        float initX = 0;
        float initY = 0;
        if (!makeBigger) {
            initX = (float) (1.1 * Selection.getBounds().left);//movedPoints[0];
            initY = (float) (1.1 * Selection.getBounds().top);//movedPoints[1];
        } else {
            initX = (float) (0.9 * Selection.getBounds().left);//movedPoints[0];
            initY = (float) (0.9 * Selection.getBounds().top);//movedPoints[1];
        }

        //x keeps track of what value we are at in the float buffer. 1 = x, 2 = y, 3 = ?
        int x = 1;
        for (int i = 0; i < movedPoints.length; i++) {
            //check if we are incrementing the x or the y value
            if (x == 1) {
                if (makeBigger) {
                    movedPoints[i] += 0.1 * movedPoints[i];
                } else {
                    movedPoints[i] -= 0.1 * movedPoints[i];
                }
                x++;
            } else if (x == 2){
                if (makeBigger) {
                    movedPoints[i] += 0.1 * movedPoints[i];
                } else {
                    movedPoints[i] -= 0.1 * movedPoints[i];
                }
                x++;
            } else {
                x = 1;
            }
        }
        //clear the point buffer and add the new moved points to the buffer
        points.clear();
        points.put(movedPoints);
        //scaling will move the stroke over, we need to move it back to its initial position
        moveElement(initX, initY, sizeOfCanvas);
    }

    public void resizeElement(float ratio, int sizeOfCanvas) {
        //convert floatBuffer to float[]
        points.flip();
        float[] movedPoints = new float[ points.limit()];
        points.get(movedPoints);

        //this is where we must move the stroke back to after spreading points out
        float initX = 0;
        float initY = 0;
            initX = (float) (ratio * getBounds().left);//movedPoints[0];
            initY = (float) (ratio * getBounds().top);//movedPoints[1];

        float offset = ratio - 1;
        //x keeps track of what value we are at in the float buffer. 1 = x, 2 = y, 3 = ?
        int x = 1;
        for (int i = 0; i < movedPoints.length; i++) {
            //check if we are incrementing the x or the y value
            if (x == 1) {
                movedPoints[i] += offset * movedPoints[i];
                x++;
            } else if (x == 2){
                movedPoints[i] += offset * movedPoints[i];
                x++;
            } else {
                x = 1;
            }
        }
        //clear the point buffer and add the new moved points to the buffer
        points.clear();
        points.put(movedPoints);
        //scaling will move the stroke over, we need to move it back to its initial position
//        moveElement(initX, initY, sizeOfCanvas);
    }

    public int getStride() {
        return stride;
    }

    public void setStride(int stride) {
        this.stride = stride;
    }

    public FloatBuffer getPoints() {
        return points;
    }

    public int getSize() {
        return size;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public float getWidth() {
        return width;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public float getStartValue() {
        return startT;
    }

    public float getEndValue() {
        return endT;
    }

    public void setInterval(float startT, float endT) {
        this.startT = startT;
        this.endT = endT;
    }

    public void setPoints(FloatBuffer points, int pointsSize) {
        size = pointsSize;
        this.points = points;
    }

    public void copyPoints(FloatBuffer source, int sourcePosition, int size) {
        this.size = size;
        points = ByteBuffer.allocateDirect(size * Float.SIZE/Byte.SIZE).order(ByteOrder.nativeOrder()).asFloatBuffer();
        Utils.copyFloatBuffer(source, points, sourcePosition, 0, size);
    }

    @Override
    public FloatBuffer getSegmentsBounds() {
        return segmentsBounds;
    }

    @Override
    public RectF getBounds() {
        return bounds;
    }

    public void setBlendMode(BlendMode blendMode){
        this.blendMode = blendMode;
    }

    public BlendMode getBlendMode() {
        return blendMode;
    }

    public void calculateBounds(){
        RectF segmentBounds = new RectF();
        Utils.invalidateRectF(bounds);
        //Allocate a float buffer to hold the segments' bounds.
        FloatBuffer segmentsBounds = Utils.createNativeFloatBuffer(PathBuilder.calculateSegmentsCount(size, stride) * 4);
        segmentsBounds.position(0);
        for (int index=0;index<PathBuilder.calculateSegmentsCount(size, stride);index++){
            PathBuilder.calculateSegmentBounds(getPoints(), getStride(), getWidth(), index, 0.0f, segmentBounds);
            segmentsBounds.put(segmentBounds.left);
            segmentsBounds.put(segmentBounds.top);
            segmentsBounds.put(segmentBounds.width());
            segmentsBounds.put(segmentBounds.height());
            Utils.uniteWith(bounds, segmentBounds);
        }
        this.segmentsBounds = segmentsBounds;
    }

    public void setPaintIndex(int paintIndex) {
        this.paintIndex = paintIndex;
    }

    public int getPaintIndex() {
        return paintIndex;
    }

    public int getSeed() {
        return seed;
    }

    public void setSeed(int seed){
        this.seed = seed;
    }

    public void setHasRandomSeed(boolean hasRandomSeed) {
        this.hasRandomSeed = hasRandomSeed;
    }

    public boolean hasRandomSeed() {
        return hasRandomSeed;
    }
}
