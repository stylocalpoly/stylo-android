package com.calpoly.stylo.canvas;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.calpoly.stylo.utils.AppUtils;
import com.wacom.ink.path.PathBuilder;
import com.wacom.ink.rasterization.BlendMode;

import java.io.InputStream;

/**
 * Created by andrewpeterson on 1/30/17.
 */

public class ElementFactory {

  private static int NO_POINTS = 0;
  private static float START_INTERVAL = 0.0f;
  private static float END_INTERVAL = 1.0f;


  public static CanvasStroke createStroke(PathBuilder pathBuilder, CanvasRenderer canvasRenderer) {
    CanvasStroke stroke = new CanvasStroke();
    stroke.copyPoints(pathBuilder.getPathBuffer(), NO_POINTS, pathBuilder.getPathSize());
    stroke.setStride(pathBuilder.getStride());
    stroke.setWidth(Float.NaN);
    stroke.setColor(canvasRenderer.getPaint().getColor());
    stroke.setInterval(START_INTERVAL, END_INTERVAL);
    stroke.setBlendMode(BlendMode.BLENDMODE_NORMAL);
    stroke.calculateBounds();
    return stroke;
  }

  public static CanvasImage createImageFromStream(InputStream input) {
    return createImageFromBitmap(BitmapFactory.decodeStream(input));
  }

  public static CanvasImage createImageFromText(String text) {
    return createImageFromBitmap(
            AppUtils.createBitmapFromString(text));
  }

  public static CanvasImage createImageFromBitmap(Bitmap bitmap) {
    return new CanvasImage(bitmap);
  }

  public static CanvasText createTextFromString(String text) {
    return new CanvasText(text);
  }



}
