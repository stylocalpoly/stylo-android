package com.calpoly.stylo.canvas;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;

import com.wacom.ink.boundary.Boundary;
import com.wacom.ink.geometry.WRect;
import com.wacom.ink.path.PathBuilder;
import com.wacom.ink.path.PathUtils;
import com.wacom.ink.path.SpeedPathBuilder;
import com.wacom.ink.utils.Utils;

import java.nio.FloatBuffer;

public class CanvasText extends CanvasBitmap {
    private String text;

    public CanvasText(String text) {
        super(ElementFactory.createImageFromText(text).toBitmap());
        this.text = text;
    }

    @Override
    public Bitmap toBitmap() {
        return ElementFactory.createImageFromText(text).toBitmap();
    }

    //Added for save
    public String getText() {
        return text;
    }
}
