package com.calpoly.stylo.canvas;

import com.calpoly.stylo.R;

/**
 * Created by roslynsierra on 1/26/17.
 */

public enum CanvasTool {
    DRAW_TOOL,
    SELECT_TOOL,
    TEXT_TOOL,
    ERASE_TOOL,
    DELETE_TOOL;

    public int getId() {
        switch (this) {
            case DRAW_TOOL:
                return R.id.drawbutton;
            case SELECT_TOOL:
                return R.id.selectionbutton;
            case TEXT_TOOL:
                return R.id.addtextbutton;
            case ERASE_TOOL:
                return R.id.erasebutton;
            default:
                return -1;
        }
    }
}
