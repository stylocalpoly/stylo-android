package com.calpoly.stylo.canvas;

import android.graphics.Bitmap;
import android.graphics.RectF;

import com.wacom.ink.geometry.WRect;
import com.wacom.ink.path.PathBuilder;
import com.wacom.ink.path.PathUtils;
import com.wacom.ink.path.SpeedPathBuilder;
import com.wacom.ink.utils.Utils;

import java.nio.FloatBuffer;

public class CanvasImage extends CanvasBitmap {
    public CanvasImage(Bitmap bitmap) {
        super(bitmap);
    }
}
