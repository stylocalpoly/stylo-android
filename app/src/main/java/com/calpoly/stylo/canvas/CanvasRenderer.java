package com.calpoly.stylo.canvas;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.SurfaceTexture;
import android.util.Log;
import android.util.Size;

import android.opengl.GLES20;
import android.support.v7.app.WindowDecorActionBar;
import android.view.MotionEvent;
import android.view.SurfaceHolder;

import com.wacom.ink.path.PathBuilder;
import com.wacom.ink.rasterization.BlendMode;
import com.wacom.ink.rasterization.InkCanvas;
import com.wacom.ink.rasterization.Layer;
import com.wacom.ink.rasterization.SolidColorBrush;
import com.wacom.ink.rasterization.StrokePaint;
import com.wacom.ink.rasterization.StrokeRenderer;
import com.wacom.ink.rendering.EGLRenderingContext;
import com.wacom.ink.willformat.aspects.ElementAspect;

import java.util.Stack;

/**
 * Handles rendering and drawing on the canvas.
 */
public class CanvasRenderer {
  public Size size;

  private InkCanvas canvas;
  private StrokeRenderer strokeRenderer;

  // Only accessed inside ElementFactory for making CanvasStroke(s).
  protected StrokePaint paint;

  private Layer viewLayer;

  /**
   * Initializes all information the CanvasRenderer needs to know to operate.
   * When called, it will create a new canvas based on the SurfaceHolder, update
   * the width and height, and initialize new variables used in the rendering.
   * @param texture
   */
  public CanvasRenderer(SurfaceTexture texture, Size size) {
    // Release the InkCanvas if it is still around.
    if (shouldReleaseResources()) {
      releaseResources();
    }


    canvas = InkCanvas.create(texture, new EGLRenderingContext.EGLConfiguration());
    this.size = size;

    // Set up the brush and paint for strokes
    SolidColorBrush brush = new SolidColorBrush();
    paint = new StrokePaint();
    paint.setStrokeBrush(brush);    // Solid color brush.
    paint.setColor(Color.BLUE);        // Blue color.

    // Create the stroke renderer
    strokeRenderer =
            new StrokeRenderer(canvas, paint, size.getWidth(), size.getHeight());

    viewLayer = canvas.createViewLayer(size.getWidth(), size.getHeight());
    canvas.clearLayer(viewLayer, Color.WHITE);
  }


  public StrokePaint getPaint() {
    return paint;
  }

  /**
   * Renders the elements to the layer, then renders the layer to the view.
   * @param layer the layer to be drawn on.
   * @param elements the stack of elements to draw.
   */
  public void render(Layer layer, Stack<CanvasElement> elements) {
    renderToView(drawElementsOn(layer, elements));
  }

  /**
   * Renders the live path to the layer, then renders the layer to the view.
   * @param layer the layer to be drawn on.
   * @param event the motion event outlining path data.
   * @param builder the path builder for constructing the path to draw.
   */
  public void renderLive(Layer layer, MotionEvent event, PathBuilder builder) {
    renderToView(drawLiveStrokeOn(layer, event, builder));
  }

  /*
   * Render the dragging of elements being moved.
   * @param the current render layer
   * @param event the motion event outlining path data.
   * @param selectedElements the elements that will be dragged
   * @param canvasSize is used for moving strokes
   */
    public Layer renderMovingLive(MotionEvent event, Stack<CanvasElement> selectedElements, Stack<CanvasElement> render, float canvasSize) {
        Layer newLayer = createLayer();
        if (event.getAction() != MotionEvent.ACTION_UP) {
            for (CanvasElement element : render) {
                if (element instanceof  CanvasStroke) {
                    CanvasStroke movingStroke = new CanvasStroke((CanvasStroke) element);
                    if (selectedElements.contains(element)) {
                        movingStroke.moveElement(event.getX(), event.getY(), (int) canvasSize);
                    }
                    drawStrokeOn(newLayer, movingStroke);
                } else {
                    CanvasBitmap movingBitmap = new CanvasBitmap((CanvasBitmap) element);
                    if (selectedElements.contains(element)) {
                        movingBitmap.moveElement(event.getX(), event.getY());
                    }
                    drawBitmapOn(newLayer, movingBitmap);
                }
            }
        }
        return newLayer;
    }

    /*
     * Render the dragging of elements being moved.
     * @param layer the current render layer
     * @param event the motion event outlining path data.
     * @param selectedElements the elements that will be dragged
     * @param canvasSize is used for moving strokes
     */
    public void renderMoving(MotionEvent event, Stack<CanvasElement> selectedElements, Stack<CanvasElement> render, float canvasSize) {
        Layer newLayer = renderMovingLive(event, selectedElements, render, canvasSize);
        renderToView(newLayer);
        newLayer.dispose();
    }

  /**
   * Creates a cleared layer based on the width and height of the canvas.
   * @return a new layer for the canvas.
   */
  public Layer createLayer() {
    Layer layer = canvas.createLayer(size.getWidth(), size.getHeight());
    clearLayer(layer);
    return layer;
  }

  /**
   * Sets the color of the strokes in the canvas.
   * @param color the new color.
   */
  public void setColor(int color) {
    paint.setColor(color);
    strokeRenderer.setStrokePaint(paint);
  }

  /**
   * Draws the stack of canvas elements onto the layer, then returns the layer.
   * @param layer the layer to draw on.
   * @param elements the elements to draw.
   * @return the layer with the elements drawn onto it.
   */
  public Layer drawElementsOn(Layer layer, Stack<CanvasElement> elements) {
    canvas.setTarget(layer);

    for (CanvasElement element : elements) {
      if (element instanceof CanvasStroke) {
        drawStrokeOn(layer, (CanvasStroke) element);
      }
      else if (element instanceof CanvasBitmap) {
        drawBitmapOn(layer, (CanvasBitmap) element);
      }
    }

    if (Selection.isSelected()) {
      drawSelectionOn(layer);
    }

    return layer;
  }

  /**
   * Draws the live stroke onto the layer.
   * This still has to be rendered to the view.
   * @param event The touch event that occurred
   */
  public Layer drawLiveStrokeOn(Layer layer, MotionEvent event, PathBuilder pathBuilder) {
    strokeRenderer.drawPoints(pathBuilder.getPathBuffer(),
            pathBuilder.getPathLastUpdatePosition(),
            pathBuilder.getAddedPointsSize(),
            pathBuilder.getStride(),
            event.getAction() == MotionEvent.ACTION_UP);

    // While drawing...
    canvas.setTarget(layer, strokeRenderer.getStrokeUpdatedArea());
    strokeRenderer.blendStrokeUpdatedArea(layer, BlendMode.BLENDMODE_NORMAL);
    return layer;
  }

  /**
   * Renders a layer to the view layer.
   * This method should be called when you have a layer with content you want the user
   * to see. To clear the canvas, you would pass in a blank layer as the argument.
   *
   * @param toRender
   */
  public void renderToView(Layer toRender) {
    canvas.setTarget(viewLayer);
    canvas.drawLayer(toRender, BlendMode.BLENDMODE_OVERWRITE);
    canvas.invalidate();
  }

  /**
   * Dispose the InkCanvas and StrokeRenderer because the Surface containing them is changing
   * or being destroyed.
   */
  public void releaseResources() {
    if (!strokeRenderer.isDisposed()) {
      strokeRenderer.dispose();
    }
    if (!canvas.isDisposed()) {
      canvas.dispose();
  }
  }

  /**
   * Clears all the layers passed to it.
   * @param layers a list of layers.
   */
  public void clear(Layer ...layers) {
    for(Layer layer : layers) {
      clearLayer(layer);
    }
  }

  private void clearLayer(Layer layer) {
    canvas.clearLayer(layer, Color.WHITE);
  }


  private void drawStrokeOn(Layer layer, CanvasStroke stroke) {
    paint.setColor(stroke.getColor());
    strokeRenderer.setStrokePaint(paint);

    strokeRenderer.drawPoints(
            stroke.getPoints(),
            0,
            stroke.getSize(),
            stroke.getStride(),
            stroke.getStartValue(),
            stroke.getEndValue(),
            true);

    strokeRenderer.blendStroke(layer, BlendMode.BLENDMODE_NORMAL);
  }


  public void drawBitmapOn(Layer layer, CanvasBitmap canvasBitmap) {
      Layer tempImageLayer = createBitmapLayer(canvasBitmap.toBitmap());

      canvasBitmap.width = tempImageLayer.getWidth();
      canvasBitmap.height = tempImageLayer.getHeight();

      canvasBitmap.calculateBounds();

      canvas.setTarget(layer, canvasBitmap.getRectangle());
      Matrix matrix = new Matrix();
      matrix.setTranslate(canvasBitmap.x, canvasBitmap.y);
      //check if we need to resize the bitmap
      if (canvasBitmap.getNewSize() != 1.0) {
          matrix.setScale(canvasBitmap.getNewSize(), canvasBitmap.getNewSize());
          canvasBitmap.width *= canvasBitmap.getNewSize();
          canvasBitmap.height *= canvasBitmap.getNewSize();
          canvasBitmap.calculateBounds();
          canvas.setTarget(layer, canvasBitmap.getRectangle());
      }

      canvas.drawLayer(tempImageLayer, matrix, BlendMode.BLENDMODE_NORMAL);
      tempImageLayer.dispose();
  }

  private void drawSelectionOn(Layer layer) {
    Layer circleLayer = createBitmapLayer(Selection.circle);
    Layer lineLayer = createBitmapLayer(Selection.line);
    Matrix matrix = new Matrix();

    Selection.calculateBounds();
    canvas.setTarget(layer);

    // Coordinates for Nodes on the right and bottom
    int right = Selection.circleRight();
    int bottom = Selection.circleBottom();

    // Draw top left
    matrix.setTranslate(Selection.x, Selection.y);
    canvas.drawLayer(circleLayer, matrix, BlendMode.BLENDMODE_NORMAL);
    // Draw top right
    matrix.setTranslate(right, Selection.y);
    canvas.drawLayer(circleLayer, matrix, BlendMode.BLENDMODE_NORMAL);
    // Draw bottom left
    matrix.setTranslate(Selection.x, bottom);
    canvas.drawLayer(circleLayer, matrix, BlendMode.BLENDMODE_NORMAL);
    // Draw bottom right
    matrix.setTranslate(right, bottom);
    canvas.drawLayer(circleLayer, matrix, BlendMode.BLENDMODE_NORMAL);

    // Coordinates for lines
    int left = Selection.lineLeft();
    int top = Selection.lineTop();
    right = Selection.lineRight();
    bottom = Selection.lineBottom();
    int widthScale = Selection.widthScale();
    int heightScale = Selection.heightScale();

    // Draw left line
    matrix.setScale(1, heightScale);
    matrix.postTranslate(left, top);
    canvas.drawLayer(lineLayer, matrix, BlendMode.BLENDMODE_NORMAL);
    // Draw right line
    matrix.setScale(1, heightScale);
    matrix.postTranslate(right, top);
    canvas.drawLayer(lineLayer, matrix, BlendMode.BLENDMODE_NORMAL);
    // Draw top line
    matrix.setScale(widthScale, 1);
    matrix.postTranslate(left, top);
    canvas.drawLayer(lineLayer, matrix, BlendMode.BLENDMODE_NORMAL);
    // Draw bottom line
    matrix.setScale(widthScale, 1);
    matrix.postTranslate(left, bottom);
    canvas.drawLayer(lineLayer, matrix, BlendMode.BLENDMODE_NORMAL);

    lineLayer.dispose();
    circleLayer.dispose();
  }


  /**
   * Create the image layer from a loaded bitmap and scale it to fit within the given width and
   * height.
   */
  private Layer createBitmapLayer(Bitmap bit) {
    // Get the image dimensions
    int imgWidth = bit.getWidth();
    int imgHeight = bit.getHeight();

    // Get the smaller of the width and height ratios
    double ratio = Math.min((double) size.getWidth() / imgWidth, (double) size.getHeight() / imgHeight);

    // If either dimension of the image is larger than the canvas
    if (ratio < 1) {
      // Scale each dimension down by the smaller ratio
      imgWidth = (int) (imgWidth * ratio);
      imgHeight = (int) (imgHeight * ratio);
    }

    // Create an image layer with the image dimensions
    Layer layer = canvas.createLayer(imgWidth, imgHeight);
    // Load the bitmap onto the image layer
    canvas.loadBitmap(layer, bit, GLES20.GL_LINEAR, GLES20.GL_CLAMP_TO_EDGE);
    return layer;
  }

  private boolean shouldReleaseResources() {
    return canvas != null && !canvas.isDisposed();
  }
}
