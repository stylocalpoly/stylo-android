package com.calpoly.stylo.canvas;

import com.wacom.ink.manipulation.Intersectable;
import com.wacom.ink.willformat.aspects.ElementAspect;

import java.io.Serializable;

public interface CanvasElement extends Intersectable, Serializable {
    void calculateBounds();
}
