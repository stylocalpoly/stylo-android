package com.calpoly.stylo.canvas;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.RectF;

import com.calpoly.stylo.utils.AppUtils;
import com.wacom.ink.geometry.WRect;
import com.wacom.ink.path.PathBuilder;
import com.wacom.ink.path.PathUtils;
import com.wacom.ink.path.SpeedPathBuilder;
import com.wacom.ink.utils.Utils;

import java.nio.FloatBuffer;

/**
 * Created by andrewpeterson on 1/31/17.
 */

public class CanvasBitmap implements CanvasElement, Cloneable {
  private Bitmap bitmap;
  private RectF bounds;
  private FloatBuffer segmentBounds;
  private FloatBuffer points;

  // These are public since the getter and setter would never have logic.
  public int x;
  public int y;

  // These are public since the getter and setter would never have logic.
  public int height;
  public int width;

  private int size;
  //this is the value that will change when resizing a canvasBitmap
  private float reSize;

  public CanvasBitmap(Bitmap bitmap) {
    this.bitmap = bitmap;
    width = bitmap.getWidth();
    height = bitmap.getHeight();
    x = 0;
    y = 0;
    reSize = 1;
  }

  public Bitmap scaleBitmap(float ratio) {
    // CREATE A MATRIX FOR THE MANIPULATION
    Matrix matrix = new Matrix();
    // RESIZE THE BIT MAP
    matrix.postScale(width * ratio, width * ratio);

    // "RECREATE" THE NEW BITMAP
    Bitmap resizedBitmap = Bitmap.createBitmap(
            bitmap, 0, 0, width, height, matrix, false);
    return resizedBitmap;
  }

  public CanvasBitmap(CanvasBitmap canvasBitmap) {
    this.bitmap = canvasBitmap.bitmap;
    this.bounds = canvasBitmap.bounds;
    this.segmentBounds = canvasBitmap.segmentBounds;
    this.points = canvasBitmap.points;
    this.x = canvasBitmap.x;
    this.y = canvasBitmap.y;
    this.width = canvasBitmap.width;
    this.height = canvasBitmap.height;
    this.size = canvasBitmap.size;
    this.reSize = canvasBitmap.reSize;
  }

  public void moveElement(float newX, float newY) {
    x = (int) newX;
    y = (int) newY;
  }

  public void resizeElement (boolean makeBigger) {
      if (makeBigger) {
          reSize += 0.1;
      } else {
          //make sure resize does not become negative
          if (reSize > 0) {
              reSize -= 0.1;
          }
      }
  }

  public void resizeElement (double ratio) {
    reSize += ratio;
  }

  public float getNewSize() {
      return reSize;
  }

  public WRect getRectangle() {
    return new WRect(x, y, width, height);
  }

  @Override
  public FloatBuffer getPoints() {
    return points;
  }

  @Override
  public int getSize() {
    return size;
  }

  @Override
  public int getStride() {
    return 2;
  }

  @Override
  public float getWidth() {
    return width;
  }

  @Override
  public float getStartValue() {
    return 0;
  }

  @Override
  public float getEndValue() {
    return 1.0f;
  }

  @Override
  public FloatBuffer getSegmentsBounds() {
    return segmentBounds;
  }

  @Override
  public RectF getBounds() {
    return bounds;
  }

  public void calculateBounds() {
    SpeedPathBuilder pb = new SpeedPathBuilder();
    pb.setNormalizationConfig(100.0f, 4000.0f);
    pb.setPropertyConfig(
            PathBuilder.PropertyName.Width,
            1f, 1f, 1f, 1f,
            PathBuilder.PropertyFunction.Power,
            1.0f,
            false);


    FloatBuffer part = pb.addPoint(PathUtils.Phase.BEGIN, x, y, 0);

    if (AppUtils.hasPartSize(pb)) {
      pb.addPathPart(part, pb.getPathPartSize());
    }

    part = pb.addPoint(PathUtils.Phase.MOVE, x + width, y, 1);

    if (AppUtils.hasPartSize(pb)) {
      pb.addPathPart(part, pb.getPathPartSize());
    }

    part = pb.addPoint(PathUtils.Phase.MOVE, x + width, y + height, 2);

    if (AppUtils.hasPartSize(pb)) {
      pb.addPathPart(part, pb.getPathPartSize());
    }

    part = pb.addPoint(PathUtils.Phase.END, x, y + height, 3);

    if (AppUtils.hasPartSize(pb)) {
      pb.addPathPart(part, pb.getPathPartSize());
    }

    points = pb.getPathBuffer();
    size = pb.getPathSize();

    bounds = new RectF(x, y, x + width, y + height);

    segmentBounds = Utils.createNativeFloatBuffer(4);
    segmentBounds.position(0);
    segmentBounds.put(x);
    segmentBounds.put(y);
    segmentBounds.put(width);
    segmentBounds.put(height);
  }

  public Bitmap toBitmap() {
    return bitmap;
  }
}
