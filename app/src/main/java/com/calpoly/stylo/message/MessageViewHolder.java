package com.calpoly.stylo.message;

/**
 * Created by andrewpeterson on 3/3/17.
 */

import android.content.Intent;
import android.graphics.SurfaceTexture;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.Size;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.TextureView;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.calpoly.stylo.CanvasActivity;
import com.calpoly.stylo.R;
import com.calpoly.stylo.SquareLinearLayout;
import com.calpoly.stylo.azure.Message;
import com.calpoly.stylo.canvas.CanvasElement;
import com.calpoly.stylo.canvas.CanvasRenderer;
import com.calpoly.stylo.canvas.Selection;
import com.calpoly.stylo.document.DocumentManager;
import com.wacom.ink.rasterization.Layer;

import java.util.Stack;

/**
 * Represents a Message.
 */
public class MessageViewHolder extends RecyclerView.ViewHolder {
  public final View view;

  private TextureView textureView;
  private TextView userName;
  private Button cloneButton;

  public Message message;
  public Layer holderLayer;
  public Stack<CanvasElement> renderStack = new Stack<>();
  public SquareLinearLayout squareLinearLayout;

  private CanvasRenderer canvasRenderer;

  public MessageViewHolder(final View view, final DocumentManager manager) {
    super(view);
    this.view = view;
    textureView = (TextureView) view.findViewById(R.id.message_texture_view);
    userName = (TextView) view.findViewById(R.id.user_name);
    squareLinearLayout = (SquareLinearLayout) view.findViewById(R.id.squarelayout);


    cloneButton = (Button) view.findViewById(R.id.clone);
    cloneButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent intent = new Intent(view.getContext(), CanvasActivity.class);
        intent.putExtra("source", message.source);
        view.getContext().startActivity(intent);
      }
    });

    // This always happens after the message field is filled.
    textureView.setSurfaceTextureListener(new TextureView.SurfaceTextureListener() {
      @Override
      public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
        Selection.initialize();

        Log.d("onSurfaceTexture", "width: " + width + ", height: " + height);

        squareLinearLayout.invalidate();
        //when selecting and item, hitting back and opening canvas again selection box still shows
        //therefore must refresh selection box
        if (Selection.isSelected()) {
          Selection.deselect();
        }


        userName.setText(message.username);

        canvasRenderer = new CanvasRenderer(surface, new Size(width, height));

        holderLayer = canvasRenderer.createLayer();

        Log.d("onBindHolder", "rendering " + message.name + " to the canvas.");

        manager.loadWillFile(renderStack, message.source, canvasRenderer.size, view.getResources().getDisplayMetrics().density);

        canvasRenderer.render(holderLayer, renderStack);

        // Clear the canvas, we only need it once.
        canvasRenderer.releaseResources();
        canvasRenderer = null;

      }

      @Override
      public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {

      }

      @Override
      public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
        return false;
      }

      @Override
      public void onSurfaceTextureUpdated(SurfaceTexture surface) {

      }
    });
  }
}