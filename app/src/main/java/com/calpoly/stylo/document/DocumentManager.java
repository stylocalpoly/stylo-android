package com.calpoly.stylo.document;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.util.Log;
import android.util.Size;

import com.calpoly.stylo.canvas.CanvasBitmap;
import com.calpoly.stylo.canvas.CanvasElement;
import com.calpoly.stylo.canvas.CanvasImage;
import com.calpoly.stylo.canvas.CanvasStroke;
import com.calpoly.stylo.canvas.CanvasText;
import com.calpoly.stylo.canvas.ElementFactory;
import com.wacom.ink.WILLException;
import com.wacom.ink.serialization.InkPathData;
import com.wacom.ink.willformat.BaseNode;
import com.wacom.ink.willformat.ContentType;
import com.wacom.ink.willformat.CorePropertiesBuilder;
import com.wacom.ink.willformat.ExtendedPropertiesBuilder;
import com.wacom.ink.willformat.Image;
import com.wacom.ink.willformat.Paths;
import com.wacom.ink.willformat.Section;
import com.wacom.ink.willformat.Text;
import com.wacom.ink.willformat.TextNode;
import com.wacom.ink.willformat.WILLFormatException;
import com.wacom.ink.willformat.WILLReader;
import com.wacom.ink.willformat.WILLWriter;
import com.wacom.ink.willformat.WillDocument;
import com.wacom.ink.willformat.WillDocumentFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.Stack;


/**
 * Created by Zack Cody on 2/12/2017.
 */

/**
 * The DocumentManager class handles the loading and saving of Will files and populating
 * either the renderStack or the newly created Will message respectively.
 */
public class DocumentManager {

    private final Context context;

    // The float precision that each stroke will have
    private static final int STROKE_PRECISION_FLOAT = 2;


    public DocumentManager(Context context) {
        this.context = context;
    }

    /**
     * This method saves the current renderStack into a Will message locally to the specified directory.
     *
     * @param renderStack The stack of all the elements that were on the canvas
     * @param source The name you want to give the Will message
     */
    public void saveWillFile(Stack<CanvasElement> renderStack, File source, Size size, Bitmap snapshot, float density) {



        // Stroke in the renderStack
        CanvasStroke stroke;
        // Will types to be stored in the message
        Text text;
        Image img;
        CanvasImage canvasImg;
        InkPathData inkPathData;
        LinkedList<InkPathData> inkPathsDataList = new LinkedList<>();
        // Document factory that uses the directory for temporary storage
        WillDocumentFactory factory = new WillDocumentFactory(context, source.getParentFile());

        try{
            // Create the new Will Document
            WillDocument willDoc = factory.newDocument();
            // Set the core properties of willDoc
            willDoc.setCoreProperties(new CorePropertiesBuilder()
                    .category("drawing")
                    .description(String.valueOf(density))
                    .created(new Date())
                    .build());
            // Set the extended properties of willDoc
            willDoc.setExtendedProperties(new ExtendedPropertiesBuilder()
                    .template("light")
                    .application("Stylo")
                    .appVersion("0.0.1")
                    .build());

            Log.d("saveWillFile", "density: " + density);
            Log.d("saveWillFile", "size: " + String.valueOf(size.getWidth()));
            Log.d("saveWillFile", "final: " + String.valueOf((size.getWidth() * density)));
            // Create a new section that will contain all the elements in the dimensions of the canvas
            Section elementsSection = willDoc.createSection()
                    .width(size.getWidth())
                    .height(size.getWidth())
                    .id("elements");

            Section snapshotSection = willDoc.createSection()
                    .width(size.getWidth())
                    .height(size.getHeight())
                    .id("snapshot");

            try {
                // Add the snapshot to the section.
                snapshotSection.addChild(
                        willDoc.createImage(snapshot, ContentType.IMAGE_PNG)
                            .x(snapshot.getWidth())
                            .y(snapshot.getHeight()));

                // For each CanvasElement that is in the current renderStack
                for(CanvasElement element: renderStack)
                {
                    // If the element is a stroke
                    if (element instanceof CanvasStroke) {
                        stroke = (CanvasStroke) element;
                        // Create a new Will specific inkPathData object to hold the stroke data
                        inkPathData = new InkPathData(
                                stroke.getPoints(),
                                stroke.getSize(),
                                stroke.getStride(),
                                stroke.getWidth(),
                                stroke.getColor(),
                                stroke.getStartValue(),
                                stroke.getEndValue(),
                                stroke.getBlendMode(),
                                stroke.getPaintIndex(),
                                stroke.getSeed(),
                                stroke.hasRandomSeed());
                        // Add the inkPathData into a list of type inkPathData
                        inkPathsDataList.add(inkPathData);
                        // Create a Will path object from the inkPathData list with specified precision
                        // Add the path to the elements section
                        elementsSection.addChild(willDoc.createPaths(inkPathsDataList, STROKE_PRECISION_FLOAT));
                        // Clear the list of inkPathData for the next stroke
                        inkPathsDataList.clear();
                    }
                    // Else if the element is an image
                    else if (element instanceof CanvasImage) {
                        canvasImg = (CanvasImage) element;
                        // Create a Will image object from the CanvasImage bitmap of type .PNG
                        // Add the image to the elements section
                        img = willDoc.createImage(canvasImg.toBitmap(), ContentType.IMAGE_PNG)
                                .x(canvasImg.x)
                                .y(canvasImg.y);
                        elementsSection.addChild(img);
                    }
                    else if (element instanceof CanvasText) {
                        // Create a Will text object
                        text = willDoc.createText();
                        // Add the String value of the CanvasText to the Will text object
                        text.addChild(willDoc.createTextNode().text(((CanvasText) element).getText()));
                        // Add the Will text to the elements section
                        elementsSection.addChild(text);
                    }
                }
            }
            catch (Exception exception) {
                exception.printStackTrace();
            }

            // Add the section to the willDoc
            willDoc.addSection(elementsSection);
            willDoc.addSection(snapshotSection);


            // Write the willDoc to the Will message
            new WILLWriter(source).write(willDoc);
            // Recycle the resources used to make the Will message
            willDoc.recycle();
        } catch (WILLFormatException e){
            throw new WILLException("Can't write the will message. Reason: " + e.getLocalizedMessage() + " / Check stacktrace in the console.");
        }
    }

    public Bitmap getSnapshot(File source) {
        try {
            // Create a Will Reader to read the Will message
            WILLReader reader = new WILLReader(new WillDocumentFactory(context, source.getParentFile()), source);
            // Read the Will message
            WillDocument doc = reader.read();
            // If there is no snapshot.
            if(doc.getSections().size() == 1) {
                return null;
            }

            // The second section is snapshots.
            Section snapshotSection = doc.getSections().get(1);
            BaseNode snapshotNode = snapshotSection.getChildren().get(0);
            return ((Image) snapshotNode).getBitmap();

        }  catch (WILLFormatException e) {
            throw new WILLException("Can't read the will message. Reason: " + e.getLocalizedMessage() + " / Check stacktrace in the console.");
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * This method loads the contents of a specified Will message into the current renderStack
     *
     * @param renderStack The stack of all the elements that were on the canvas
     * @param source The name of the Will message you want to read
     */
    public void loadWillFile(Stack<CanvasElement> renderStack, File source, Size renderSize, float density) {
        CanvasImage canvasImg;
        // Load the Will message from the specified directory and filename
        try {
            // Create a Will Reader to read the Will message
            WILLReader reader = new WILLReader(new WillDocumentFactory(context, source.getParentFile()), source);
            // Read the Will message
            WillDocument doc = reader.read();

            // Get the elements section from the message
            //Section elementsSection = doc.getSectionById("elements");
            Section elementsSection = doc.getSections().get(0);

            Size oldSize = new Size((int) elementsSection.getWidth(), (int) elementsSection.getHeight());
            // Get all the children from the section that are the elements
            ArrayList<BaseNode> elements = elementsSection.getChildren();

            Log.d("loadWillFile", "old: " + oldSize.toString());
            Log.d("loadWillFile", "dpi: " + density);
            Log.d("loadWillFile", "newSize: " + renderSize.getWidth());
//            float newPixels = renderSize.getWidth() * density;

            double oldDensity;
            if(doc.getCoreProperties().description != null) {
                oldDensity = Double.parseDouble(doc.getCoreProperties().description);
            } else {
                oldDensity = 1;
            }

            double old = oldSize.getWidth() * (oldDensity / density) * oldDensity;
            double ratio = (float) (renderSize.getWidth() * density) / old;


            Log.d("loadWillFile", "ratio: " + String.valueOf(ratio));
            double offset = ratio - 1;
            Log.d("offset", String.valueOf(offset));
            // For each element in the list of BaseNode objects
            for (BaseNode node : elements) {
                // If the node is a stroke
                if (node.getType() == BaseNode.TYPE_PATHS) {
                    // Cast it to a Path object
                    Paths pathsElement = (Paths)node;
                    // For each InkPathData in the path (Should only be one)
                    for (InkPathData inkPath : pathsElement.getInkPaths()) {
                        // Make a new CanvasStroke object
                        CanvasStroke stroke = new CanvasStroke();
                        // Set all the fields in CanvasStroke
                        stroke.copyPoints(inkPath.getPoints(), 0, inkPath.getSize());
                        stroke.setStride(inkPath.getStride());
                        stroke.setWidth(inkPath.getWidth());
                        stroke.setBlendMode(inkPath.getBlendMode());
                        stroke.setInterval(inkPath.getTs(), inkPath.getTf());
                        stroke.setColor(inkPath.getColor());
                        stroke.setPaintIndex(inkPath.getPaintIndex());
                        stroke.setSeed(inkPath.getRandomSeed());
                        stroke.setHasRandomSeed(inkPath.hasRandomSeed());
                        stroke.calculateBounds();
                        // Add the CanvasStroke to the renderStack

                        stroke.resizeElement((float)ratio, renderSize.getWidth());
                        renderStack.add(stroke);
                    }
                }
                // Else if the node is an image
                else if (node.getType() == BaseNode.TYPE_IMAGE) {

                    double tempDensity;
                    if(doc.getCoreProperties().description != null) {
                        tempDensity= Double.parseDouble(doc.getCoreProperties().description);

                        ratio = (float) (renderSize.getWidth() * density) / old;
                    } else {
                        tempDensity = 1;
                        ratio = 1;
                    }

                    float viewPortRatio = (float) renderSize.getWidth() / ((float)(oldSize.getWidth() * tempDensity) / density) ;
                    float densityIndependentRatio = (float) renderSize.getWidth() / (float) oldSize.getWidth();
                    // Create a CanvasImage object from the bitmap of the node
                    // Add the CanvasImage to the renderStack


                    Image image = (Image) node;
                    Bitmap bitmap = image.getBitmap();

                    Bitmap resized = Bitmap.createScaledBitmap(bitmap, (int) (bitmap.getWidth() * viewPortRatio), (int) (bitmap.getHeight() * viewPortRatio), false);

                        canvasImg = ElementFactory.createImageFromBitmap(resized);
                        canvasImg.x = (int) (image.getX() * densityIndependentRatio);
                        canvasImg.y = (int) (image.getY() * densityIndependentRatio);

                        renderStack.add(canvasImg);
                }
                // Else if the node is text
                else if (node.getType() == BaseNode.TYPE_TEXT) {
                    // Get the text wrapper from the node
                    ArrayList<BaseNode> texts = ((Text) node).findChildren(BaseNode.TYPE_TEXTNODE);
                    // For each text section in the wrapper
                    for (BaseNode word : texts) {
                        // Create a CanvasText object from the String of the node
                        // Add the CanvasText to the renderStack
                        CanvasText text = ElementFactory.createTextFromString(((TextNode) word).getText());
                        text.resizeElement(offset);
                        renderStack.add(text);
                    }

                    Log.d("ImageRender", "Old: " + oldSize.toString() + ", New: " + renderSize.toString());
                }

                if (doc.getSections().size() > 1) {
                }
            }
            // Delete the temporary resources to read the message
            doc.recycle();

        }
        catch (WILLFormatException e) {
            throw new WILLException("Can't read the will message. Reason: " + e.getLocalizedMessage() + " / Check stacktrace in the console.");
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
