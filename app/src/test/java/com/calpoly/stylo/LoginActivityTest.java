package com.calpoly.stylo;

import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;

import com.calpoly.stylo.azure.AppDAO;
import com.calpoly.stylo.azure.Session;
import com.calpoly.stylo.group.CurrentUser;
import com.calpoly.stylo.group.User;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.ListenableFuture;
import com.microsoft.windowsazure.mobileservices.authentication.CustomTabsIntermediateActivity;
import com.microsoft.windowsazure.mobileservices.authentication.MobileServiceAuthenticationProvider;

import org.assertj.android.api.content.IntentAssert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.rule.PowerMockRule;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowActivity;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.powermock.api.mockito.PowerMockito.doNothing;
import static org.powermock.api.mockito.PowerMockito.verifyStatic;
import static org.robolectric.Shadows.shadowOf;

/**
 * Tests the buttons on the Login Activity using a Mock Session.
 * Does not test any calls to the backend.
 *
 *
 * The RunWith, Config, and PowerMockIgnore annotations are necessary.
 * The PrepareForTest annotation is used to prepare the Session class for mocking.
 */
@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, sdk = Build.VERSION_CODES.N)
@PowerMockIgnore({"org.mockito.*", "org.robolectric.*", "android.*"})
@PrepareForTest({Session.class, AppDAO.class})
public class LoginActivityTest {

    // The Login Activity instance
    private static LoginActivity activity;

    // Needed for PowerMock
    @Rule
    public PowerMockRule rule = new PowerMockRule();

    /**
     * Before the tests, mock Session and set up the Activity.
     */
    @Before
    public void setup() {
        PowerMockito.mockStatic(Session.class);
        PowerMockito.mockStatic(AppDAO.class);
        activity = Robolectric.setupActivity(LoginActivity.class);
        try {
            PowerMockito.doNothing().when(AppDAO.class, "initialize", any(Context.class));
            PowerMockito.doNothing().when(AppDAO.class, "clearTables");
            PowerMockito.doNothing().when(AppDAO.class, "setCurrentUser", any(CurrentUser.class), any(FutureCallback.class));
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    /**
     * Test the login button and the login callback for the Microsoft OAuth Provider.
     */
    @Test
    public void microsoftLogin() {
        // Make Session return a fake callback
        String myProvider = "Microsoft";
        PowerMockito.when(Session.login(MobileServiceAuthenticationProvider.MicrosoftAccount))
                .thenReturn(new UserFuture(myProvider));

        // Test the button and verify a user with the id Microsoft was created
        activity.findViewById(R.id.microsoftbutton).performClick();
        activity.setCallback.onSuccess(null);

        IntentAssert intentAssert = new IntentAssert(shadowOf(activity).getNextStartedActivity());
        intentAssert.hasComponent(activity, GroupListActivity.class);
        assertEquals(Session.user.getId(), myProvider);
    }

    /**
     * Test the login button and the login callback for the Google OAuth Provider.
     * Same as the microsoftLogin Test, but for Google.
     */
    @Test
    public void googleLogin() {
        String myProvider = "Google";

        //TODO: Fix Google Login Test
        activity.findViewById(R.id.googlebutton).performClick();
        activity.setCallback.onSuccess(null);

        IntentAssert intentAssert = new IntentAssert(shadowOf(activity).getNextStartedActivity());
        intentAssert.hasComponent(activity, GroupListActivity.class);
        assertEquals(Session.user.getId(), myProvider);

    }

    /**
     * Test the login button and the login callback for the Facebook OAuth Provider.
     * Same as the microsoftLogin Test, but for Facebook.
     */
    @Test
    public void facebookLogin() {
        String myProvider = "Facebook";
        PowerMockito.when(Session.login(MobileServiceAuthenticationProvider.Facebook))
                .thenReturn(new UserFuture(myProvider));

        activity.findViewById(R.id.facebookbutton).performClick();
        activity.setCallback.onSuccess(null);

        IntentAssert intentAssert = new IntentAssert(shadowOf(activity).getNextStartedActivity());
        intentAssert.hasComponent(activity, GroupListActivity.class);
        assertEquals(Session.user.getId(), myProvider);
    }

    /**
     * A Fake Listenable Future that creates a user with the given string as the id.
     */
    private class UserFuture implements ListenableFuture<User> {
        private String provider;

        public UserFuture(String authProvider) {
            provider = authProvider;
        }

        @Override
        public void addListener(Runnable runnable, Executor executor) {
            executor.execute(runnable);
        }

        @Override
        public boolean cancel(boolean mayInterruptIfRunning) {
            return true;
        }

        @Override
        public boolean isCancelled() {
            return false;
        }

        @Override
        public boolean isDone() {
            return true;
        }

        @Override
        public User get() throws InterruptedException, ExecutionException {
            return new User(provider);
        }

        @Override
        public User get(long timeout, @NonNull TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
            return new User(provider);
        }
    }
}