package com.calpoly.stylo;

import android.app.Dialog;
import android.arch.lifecycle.LiveData;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.annotation.NonNull;
import android.view.MenuItem;
import android.widget.EditText;

import com.calpoly.stylo.azure.AppDAO;
import com.calpoly.stylo.azure.OAuthService;
import com.calpoly.stylo.azure.Session;
import com.calpoly.stylo.group.Group;
import com.google.common.util.concurrent.ListenableFuture;

import org.assertj.android.api.content.IntentAssert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.rule.PowerMockRule;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.android.controller.ActivityController;
import org.robolectric.android.controller.ComponentController;
import org.robolectric.annotation.Config;
import org.robolectric.fakes.RoboMenuItem;
import org.robolectric.shadows.ShadowActivity;
import org.robolectric.shadows.ShadowDialog;
import org.robolectric.shadows.ShadowToast;

import java.lang.reflect.Field;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.spy;
import static org.powermock.api.mockito.PowerMockito.doNothing;
import static org.robolectric.Shadows.shadowOf;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, sdk = Build.VERSION_CODES.N)
@PowerMockIgnore({"org.mockito.*", "org.robolectric.*", "android.*"})
@PrepareForTest({Session.class, AppDAO.class})
public class GroupListActivityTest {
    private static GroupListActivity activity;

    @Rule
    public PowerMockRule rule = new PowerMockRule();

    @Before
    public void setup() {
        PowerMockito.mockStatic(Session.class);
        PowerMockito.mockStatic(AppDAO.class);
        ActivityController<GroupListActivity> controller = Robolectric.buildActivity(GroupListActivity.class);
        final GroupListActivity mockActivity = spy(controller.get());
        doNothing().when(mockActivity).registerWithNotificationHubs();
        PowerMockito.when(AppDAO.getGroups()).thenReturn(PowerMockito.mock(LiveData.class));

        try {
            Field component = ComponentController.class.getDeclaredField("component");
            component.setAccessible(true);
            component.set(controller, mockActivity);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        activity = controller.create().get();
    }

    @Test
    public void createGroupDialogTest() {
        PowerMockito.when(Session.createGroup(any(Group.class), any(Context.class))).thenReturn(new CreateGroupFuture("Android Test Group"));
        activity.findViewById(R.id.create_group_button).performClick();
        Dialog groupDialog = ShadowDialog.getLatestDialog();
        EditText groupNameInput = (EditText) groupDialog.findViewById(R.id.edit_text);
        groupNameInput.setText("Android Test Group");
        groupDialog.findViewById(R.id.finish_group_creation_button).performClick();

        assertFalse(groupDialog.isShowing());

        activity.findViewById(R.id.create_group_button).performClick();
        groupDialog = ShadowDialog.getLatestDialog();
        groupNameInput = (EditText) groupDialog.findViewById(R.id.edit_text);
        groupNameInput.setText("");
        groupDialog.findViewById(R.id.finish_group_creation_button).performClick();

        assertTrue(groupDialog.isShowing());
        assertEquals(ShadowToast.getTextOfLatestToast(), "Groups must have a name.");
    }

    @Test public void logOutTest() {
        SharedPreferences prefs = activity.getSharedPreferences(LoginActivity.SHAREDPREFFILE, Context.MODE_PRIVATE);
        String testUser = "User";
        String testToken = "Token";

        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(OAuthService.USERIDPREF, testUser);
        editor.putString(OAuthService.TOKENPREF, testToken);
        editor.apply();

        assertEquals(testUser, prefs.getString(OAuthService.USERIDPREF, null));
        assertEquals(testToken, prefs.getString(OAuthService.TOKENPREF, null));

        MenuItem menuItem = new RoboMenuItem(R.id.logout_button);
        activity.onOptionsItemSelected(menuItem);

        ShadowActivity shadowGroupList = shadowOf(activity);


        IntentAssert intentAssert = new IntentAssert(shadowGroupList.getNextStartedActivity());
        intentAssert.hasComponent(activity, LoginActivity.class);
        assertTrue(shadowGroupList.isFinishing());

        assertNull(prefs.getString(OAuthService.USERIDPREF, null));
        assertNull(prefs.getString(OAuthService.TOKENPREF, null));
    }

    private class CreateGroupFuture implements ListenableFuture<Group> {
        private String name;

        public CreateGroupFuture(String name){this.name = name;}

        @Override
        public void addListener(Runnable runnable, Executor executor) {
            executor.execute(runnable);
        }

        @Override
        public boolean cancel(boolean mayInterruptIfRunning) {
            return true;
        }

        @Override
        public boolean isCancelled() {
            return false;
        }

        @Override
        public boolean isDone() {
            return true;
        }

        @Override
        public Group get() throws InterruptedException, ExecutionException {
            return new Group(name);
        }

        @Override
        public Group get(long timeout, @NonNull TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
            return new Group(name);
        }
    }
}