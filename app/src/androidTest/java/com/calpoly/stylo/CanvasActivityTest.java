package com.calpoly.stylo;

import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.support.test.espresso.Espresso;
import android.support.test.rule.ActivityTestRule;
import android.util.Log;

import com.calpoly.stylo.canvas.CanvasElement;
import com.calpoly.stylo.canvas.CanvasStroke;
import com.calpoly.stylo.canvas.CanvasText;
import com.calpoly.stylo.canvas.CanvasTool;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.*;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;

import java.util.Stack;

/**
 * Created by roslynsierra on 2/3/17.
 */

public class CanvasActivityTest {
    
    @Rule
    public ActivityTestRule<CanvasActivity> activityTestRule = new ActivityTestRule<>(CanvasActivity.class, true, true);

    private CanvasActivity canvas;

    @Before
    public void setUp() {
        //open the canvas
        canvas = activityTestRule.getActivity();
    }

  /**
   * Draws a stroke on the current canvas, used in test setup.
   */
  private void drawStroke() {
        onView(withId(R.id.activity_canvas)).perform(swipeRight());
    }

   /* @Test
    public void testDrawOnCanvas() {
        //check that the canvas is empty
        assertTrue(canvas.getRenderStack().empty());
        assertTrue(canvas.getUndoHistory().empty());
        //draw a stroke on canvas
        drawStroke();
        //check that a stroke has been added to the canvas
        assertFalse(canvas.getRenderStack().empty());
        assertTrue(canvas.getRenderStack().peek() instanceof CanvasStroke);

        //click undo button
        onView(withId(R.id.undobutton)).perform(click());
        //check that the canvas is now empty
        assertTrue(canvas.getRenderStack().empty());
    }

  private void drawText(String text) {
    onView(withId(R.id.addtextbutton)).perform(click());
    onView(withId(R.id.editText)).perform(typeTextIntoFocusedView(text));
    onView(withId(R.id.setTextButton)).perform(click());
  }

    @Test
  public void testDrawText() {
    String input = "Test";

    drawText(input);

    CanvasText canvasText = (CanvasText) canvas.getRenderStack().peek();
    assertTrue(canvasText.getText().equals(input));

    onView(withId(R.id.undobutton)).perform(click());
    assertTrue(canvas.getRenderStack().isEmpty());
  }*/

  @Test
  public void testErase() {
    drawStroke();

    assertEquals(canvas.getRenderStack().size(), 1);
    Stack<CanvasElement> oldStack = canvas.getRenderStack();
    CanvasElement elem = oldStack.peek();

    onView(withId(R.id.erasebutton)).perform(click());
    onView(withId(R.id.activity_canvas)).perform(swipeUp());

    assertEquals(2, canvas.getRenderStack().size());

    assertNotEquals(elem, canvas.getRenderStack().pop());
    assertNotEquals(elem, canvas.getRenderStack().pop());

  }
}
