package com.calpoly.stylo;

import android.support.test.rule.ActivityTestRule;
import android.util.Log;

import com.calpoly.stylo.canvas.CanvasStroke;
import com.calpoly.stylo.canvas.CanvasText;
import com.calpoly.stylo.canvas.CanvasTool;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.pressKey;
import static android.support.test.espresso.action.ViewActions.swipeRight;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.view.KeyEvent.KEYCODE_ENTER;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by roslynsierra on 2/14/17.
 */

public class ToolbarTest {
    @Rule
    public ActivityTestRule<CanvasActivity> activityTestRule = new ActivityTestRule<>(CanvasActivity.class, true, true);

    private CanvasActivity canvas;

    @Before
    public void setUp() {
        //open the canvas
        canvas = activityTestRule.getActivity();
    }

    /**
     * Draws a stroke on the current canvas, used in test setup.
     */
    private void drawStroke() {
        onView(withId(R.id.activity_canvas)).perform(swipeRight());
    }

    @Test
    public void testToolbarButtons() {
        assertTrue(canvas.getCanvasTool() == CanvasTool.DRAW_TOOL);
        //click erase button
        onView(withId(R.id.erasebutton)).perform(click());
        assertTrue(canvas.getCanvasTool() == CanvasTool.ERASE_TOOL);
        //click text button
        onView(withId(R.id.addtextbutton)).perform(click());
        assertTrue(canvas.getCanvasTool() == CanvasTool.TEXT_TOOL);
        onView(withId(R.id.setTextButton)).perform(click());
        //click select button
        onView(withId(R.id.selectionbutton)).perform(click());
        assertTrue(canvas.getCanvasTool() == CanvasTool.SELECT_TOOL);
        //click image button
        onView(withId(R.id.undobutton)).perform(click());
        onView(withId(R.id.undobutton)).perform(click());
        assertTrue(canvas.getRenderStack().isEmpty());
    }
}
